package homework.dz_1_1.task_3;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;


class MainTest {


    /**
     * Метод testNameValidInput(), который проверяет, что метод
     * name корректно обрабатывает валидный ввод - строку,
     * состоящую из имени.
     */
    @Test
    // позволяет задать читаемое имя тесту, которое будет
    // отображаться при выводе результатов выполнения тестов.
    @DisplayName("Тестирование метода name при вводе корректного имени")
    void testNameValidInput() {

        Main main = new Main();
        String input = "Иван";
        // ByteArrayInputStream позволяет считывать данные из
        // массива байтов, передаваемых ему в качестве аргумента в
        // конструкторе. Метод getBytes класса String возвращает
        // массив байтов, представляющий данную строку в кодировке UTF-8.
        InputStream in = new ByteArrayInputStream(input.getBytes(
                StandardCharsets.UTF_8));
        // По умолчанию, System.in считывает ввод с клавиатуры,
        // но с помощью метода System.setIn можно изменить его,
        // чтобы ввод был считан из другого источника.
        // В данном случае, мы создаем ByteArrayInputStream,
        // который содержит вводные данные для тестового метода,
        // и затем устанавливаем его в System.in. Таким образом,
        // при вызове Scanner.nextLine() в тестовом методе,
        // вместо считывания данных с клавиатуры,
        // будут считаны данные из ByteArrayInputStream,
        // который мы установили в качестве System.in.
        System.setIn(in);
        // проверяется, что результат выполнения метода равен
        // ожидаемому значению "Иван"
        Assertions.assertEquals("Иван", main.name(input));
    }


    /**
     * Метод testNameInvalidInput(), который проверяет, что метод name
     * корректно обрабатывает недопустимый ввод. Строку,
     * длина которой превышает 99 символов.
     */
    @Test
    @DisplayName("Тестирование метода name при вводе имени, " +
            "длина которого превышает 99 символов")
    void testNameInvalidInput() {

        Main main = new Main();
        // Метод repeat(int count) является методом класса String,
        // который повторяет данную строку указанное количество раз
        // (в данном случае, 100 раз). Таким образом, input будет
        // содержать 100 символов "а".
        String input = "a".repeat(100);
        InputStream in = new ByteArrayInputStream(input.getBytes(
                StandardCharsets.UTF_8));
        System.setIn(in);
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> main.name(input));
    }
}