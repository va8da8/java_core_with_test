package homework.dz_1_1.task_5;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


class MainTest {


    @Test
    @DisplayName("Тестирование метода inchesToCentimeters" +
            " с корректными входными параметрами")
    void testInchesToCentimetersValidInput() {

        Main main = new Main();
        int inches = 99;
        double expected = 251.46;
        double actual = main.inchesToCentimeters(inches, Main.inch);
        Assertions.assertEquals(expected, actual, 0.01,
                "Конвертация дюймов в сантиметры прошла" +
                        " некорректно");
    }


    @Test
    @DisplayName("Тестирование метода inchesToCentimeters" +
            " с некорректным значением дюймов")
    void testInchesToCentimetersInvalidInput() {

        Main main = new Main();
        int inches = 0;
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> main.inchesToCentimeters(inches, Main.inch),
                "Метод не бросил исключение" +
                        " IllegalArgumentException при введении" +
                        " некорректного значения");
    }
}