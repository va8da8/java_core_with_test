package homework.dz_1_1.task_9;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


class MainTest {


    @Test
    @DisplayName("Тестирование метода количествоГостей при" +
            " корректных входных данных")
    void testValidInput() {

        Main main = new Main();
        Assertions.assertEquals(957, main.количествоГостей(
                85177, 89));
    }


    @Test
    @DisplayName("Тестирование метода количествоГостей при" +
            " неправильном количестве гостей")
    void testInvalidNumberOfGuests() {

        Main main = new Main();
        IllegalArgumentException exception =
                Assertions.assertThrows(IllegalArgumentException.class,
                        () -> main.количествоГостей(0, 1));
        Assertions.assertEquals("Введено недопустимое значение",
                exception.getMessage());
    }


    @Test
    @DisplayName("Тестирование метода количествоГостей при" +
            " неправильном количестве приглашенных")
    void testInvalidNumberOfInvited() {

        Main main = new Main();
        IllegalArgumentException exception =
                Assertions.assertThrows(IllegalArgumentException.class,
                        () -> main.количествоГостей(10, 0));
        Assertions.assertEquals("Введено недопустимое значение",
                exception.getMessage());
    }


    @Test
    @DisplayName("Тестирование метода количествоГостей при" +
            " приглашенных больше, чем гостей")
    void testInvalidInput() {

        Main main = new Main();
        IllegalArgumentException exception =
                Assertions.assertThrows(IllegalArgumentException.class,
                        () -> main.количествоГостей(5, 10));
        Assertions.assertEquals("Введено недопустимое значение",
                exception.getMessage());
    }
}