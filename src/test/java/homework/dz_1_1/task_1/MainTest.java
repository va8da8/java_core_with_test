package homework.dz_1_1.task_1;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class MainTest {

    // создается новый экземпляр класса Main, который
    // содержит метод ballVolume()
    Main main = new Main();


    /**
     * Метод testBallVolumeWithValidRadius(), который проверяет,
     * возвращает ли метод ballVolume() правильный объем шара для
     * допустимого значения радиуса.
     */
    // аннотация, указывающая на то, что этот метод является тестом
    @Test
    public void testBallVolumeWithValidRadius() {

        // вызывается метод ballVolume() с радиусом 9,
        // чтобы получить объем шара
        double volume = main.ballVolume(9);
        // Проверяет, что значение volume равно 3053.6 с точностью до 0.1.
        // означает, что метод ballVolume() должен вернуть число,
        // которое находится в пределах 0.1 от 3053.6. Если значение
        // volume не соответствует ожидаемому значению, то тест
        // завершится неудачей.
        assertEquals(3053.6, volume, 0.1);
    }


    /**
     * Метод testBallVolumeWithMinimumValidRadius(), который проверяет,
     * возвращает ли метод ballVolume() правильный объем шара для
     * минимального допустимого значения радиуса.
     */
    @Test
    public void testBallVolumeWithMinimumValidRadius() {

        double volume = main.ballVolume(1);
        assertEquals(4.2, volume, 0.1);
    }


    /**
     * Метод testBallVolumeWithMaximumValidRadius(), который проверяет,
     * возвращает ли метод ballVolume() правильный объем шара для
     * максимального допустимого значения радиуса.
     */
    @Test
    public void testBallVolumeWithMaximumValidRadius() {

        double volume = main.ballVolume(99);
        assertEquals(4064378.9, volume, 0.1);
    }


    /**
     * Метод testBallVolumeWithInvalidRadius(), который проверяет,
     * вызывает ли метод ballVolume() исключение IllegalArgumentException,
     * если передается недопустимое значение радиуса. Также проверяет,
     * что сообщение об ошибке содержит правильный текст.
     */
    @Test
    public void testBallVolumeWithInvalidRadius() {

        // вызывается метод ballVolume() с недопустимым радиусом 100
        // и ожидается, что он генерирует исключение типа
        // IllegalArgumentException. Если метод не генерирует
        // исключение, то тест завершится неудачей
        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> main.ballVolume(100));
        // ожидаемое сообщение исключения
        String expectedMessage = "Введено недопустимое значение";
        // получаем сообщение, которое сгенерировало исключение
        String actualMessage = exception.getMessage();
        // проверяем, что сообщение, сгенерированное исключением,
        // содержит ожидаемое сообщение.
        assertTrue(actualMessage.contains(expectedMessage));
    }
}