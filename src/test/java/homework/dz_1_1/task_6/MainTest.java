package homework.dz_1_1.task_6;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


class MainTest {


    @Test
    @DisplayName("Тестирование метода kilometersToMiles при" +
            " вводе корректных данных")
    void testKilometersToMilesValidInput() {

        Main main = new Main();
        int input = 143;
        Assertions.assertEquals(88.85630134092237,
                main.kilometersToMiles(input, Main.mile));
    }


    @Test
    @DisplayName("Тестирование метода kilometersToMiles при " +
            "вводе данных, не входящих в допустимый диапазон")
    void testKilometersToMilesInvalidInput() {

        Main main = new Main();
        int input = 0;
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> main.kilometersToMiles(input, Main.mile));
    }
}