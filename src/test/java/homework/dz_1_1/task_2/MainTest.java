package homework.dz_1_1.task_2;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class MainTest {

    Main main = new Main();


    @Test
    void testRootMeanSquareWithValidInputs() {

        double volume = main.rootMeanSquare(23, 70);
        assertEquals(52.1008, volume, 0.0001);
    }


    @Test
    void testRootMeanSquareWithOneInvalidInput() {

        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> main.rootMeanSquare(100, 0));
        String expectedMessage = "Введено недопустимое значение";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }


    @Test
    void testRootMeanSquareWithBothInvalidInputs() {

        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> main.rootMeanSquare(0, 100));
        String expectedMessage = "Введено недопустимое значение";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }
}