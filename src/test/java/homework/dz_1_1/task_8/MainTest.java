package homework.dz_1_1.task_8;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


class MainTest {


    @Test
    void testDailyBudgetValidInput() {

        Main main = new Main();
        int n = 13509;
        double expected = 450.3;
        Assertions.assertEquals(expected, main.dailyBudget(n, 30));
    }


    @Test
    void testDailyBudgetLowerBound() {

        Main main = new Main();
        int n = 1;
        double expected = n / 30.0;
        Assertions.assertEquals(expected, main.dailyBudget(n, 30));
    }


    @Test
    void testDailyBudgetUpperBound() {

        Main main = new Main();
        int n = 99999;
        double expected = n / 30.0;
        Assertions.assertEquals(expected, main.dailyBudget(n, 30));
    }

    @Test
    void testDailyBudgetInvalidInput() {

        Main main = new Main();
        int n = -1;
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> main.dailyBudget(n, 30));
    }
}