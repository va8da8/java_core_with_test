package homework.dz_1_1.task_4;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;


class MainTest {


    @Test
    @DisplayName("Тестирование метода time при вводе корректного" +
            " значения")
    void testTimeValidInput() {

        Main main = new Main();
        int input = 41812;
        InputStream in = new ByteArrayInputStream(String.valueOf(input)
                .getBytes(StandardCharsets.UTF_8));
        System.setIn(in);
        Assertions.assertEquals("11 36", main.time(input));
    }


    @Test
    @DisplayName("Тестирование метода time при вводе значения меньше 1")
    void testTimeInvalidInputLessThan1() {

        Main main = new Main();
        int input = 0;
        InputStream in = new ByteArrayInputStream(String.valueOf(input)
                .getBytes(StandardCharsets.UTF_8));
        System.setIn(in);
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> main.time(input));
    }


    @Test
    @DisplayName("Тестирование метода time при вводе значения " +
            "больше 86399")
    void testTimeInvalidInputGreaterThan86399() {

        Main main = new Main();
        int input = 86400;
        InputStream in = new ByteArrayInputStream(String.valueOf(input)
                .getBytes(StandardCharsets.UTF_8));
        System.setIn(in);
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> main.time(input));
    }
}