package homework.dz_1_1.task_7;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


class MainTest {


    @Test
    @DisplayName("Тестирование метода permutationNumbers при" +
            " вводе корректного значения")
    void testPermutationNumbersValidInput() {

        Main main = new Main();
        int n = 45;
        Assertions.assertEquals("54",
                main.permutationNumbers(n));
    }


    @Test
    @DisplayName("Тестирование метода permutationNumbers при" +
            " вводе значения меньше 10")
    void testPermutationNumbersInvalidInputLessThan10() {

        Main main = new Main();
        int n = 5;
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> main.permutationNumbers(n));
    }


    @Test
    @DisplayName("Тестирование метода permutationNumbers при" +
            " вводе значения больше 99")
    void testPermutationNumbersInvalidInputMoreThan99() {

        Main main = new Main();
        int n = 100;
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> main.permutationNumbers(n));
    }
}