package homework.dz_1_3.task_9;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;

import static org.junit.jupiter.api.Assertions.*;


class MainTest {

    Main main = new Main();


    @Test
    @DisplayName("Тест, когда все введенные числа положительные.")
    public void testSumNegativeNumbers_allPositiveNumbers() {

        ByteArrayInputStream in =
                new ByteArrayInputStream("10 20".getBytes());
        System.setIn(in);
        int result = main.sumNegativeNumbers();
        assertEquals(0, result);
    }


    @Test
    @DisplayName("Тест на ввод отрицательных чисел, до положительного.")
    public void testSumNegativeNumbers_allNegativeNumbers() {

        ByteArrayInputStream in =
                new ByteArrayInputStream("-55 -42 -19 -15 17 33"
                        .getBytes());
        System.setIn(in);
        int result = main.sumNegativeNumbers();
        assertEquals(4, result);
    }


    @Test
    @DisplayName("Тест для генерации исключения " +
            "IllegalArgumentException недопустимого ввода")
    void testSumNegativeNumbersInvalidInput() {

        ByteArrayInputStream in =
                new ByteArrayInputStream("-1000".getBytes());
        System.setIn(in);
        assertThrows(IllegalArgumentException.class,
                () -> main.sumNegativeNumbers());
    }
}