package homework.dz_1_3.task_8;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class MainTest {

    Main main = new Main();


    @Test
    @DisplayName("Тест на сумму чисел, которые больше числа р")
    public void testSumOfNumbersGreaterThanP() {

        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        assertEquals(40,
                main.sumOfNumbersGreaterThanP(10, 5, arr));
        assertEquals(0,
                main.sumOfNumbersGreaterThanP(5, 10, arr));
        assertEquals(27,
                main.sumOfNumbersGreaterThanP(10, 7, arr));
    }


    @Test
    @DisplayName("Тест на неверный ввод")
    public void testInvalidInput() {

        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        assertThrows(IllegalArgumentException.class,
                () -> main.sumOfNumbersGreaterThanP(-1, 5, arr));
        assertThrows(IllegalArgumentException.class,
                () -> main.sumOfNumbersGreaterThanP(0, 5, arr));
        assertThrows(IllegalArgumentException.class,
                () -> main.sumOfNumbersGreaterThanP(1001, 5, arr));
        assertThrows(IllegalArgumentException.class,
                () -> main.sumOfNumbersGreaterThanP(5, -1, arr));
        assertThrows(IllegalArgumentException.class,
                () -> main.sumOfNumbersGreaterThanP(5, 0, arr));
        assertThrows(IllegalArgumentException.class,
                () -> main.sumOfNumbersGreaterThanP(5, 1000, arr));
        int[] arr2 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 1000};
        assertThrows(IllegalArgumentException.class,
                () -> main.sumOfNumbersGreaterThanP(10, 5, arr2));
        int[] arr3 = {0, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        assertThrows(IllegalArgumentException.class,
                () -> main.sumOfNumbersGreaterThanP(10, 5, arr3));
    }
}