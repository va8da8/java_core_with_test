package homework.dz_1_3.task_7;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class MainTest {

    Main main = new Main();


    @Test
    @DisplayName("Длина строки должна быть корректно вычислена")
    void testStringLength() {

        String str = "Hello world.";
        int expectedLength = 11;
        assertEquals(expectedLength, main.stringLength(str));
    }


    @Test
    @DisplayName("Должно выбрасываться исключение " +
            "IllegalArgumentException при вводе слишком длинной строки")
    void testIllegalArgumentException() {

        String str = "Hello ".repeat(200);
        assertThrows(IllegalArgumentException.class, () ->
            main.stringLength(str));
    }
}