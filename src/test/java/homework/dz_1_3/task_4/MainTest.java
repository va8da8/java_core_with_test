package homework.dz_1_3.task_4;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;


class MainTest {


    @Test
    @DisplayName("Тест на ввод числа меньше 1")
    void testNumbersInAColumnLessThanOne() {

        Exception exception = assertThrows(
                IllegalArgumentException.class,
                () -> Main.numbersInAColumn(0));

        String expectedMessage = "Введено недопустимое значение";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }


    @Test
    @DisplayName("Тест на ввод числа больше 999_999")
    void testNumbersInAColumnMoreThanOneMillion() {

        Exception exception = assertThrows(
                IllegalArgumentException.class,
                () -> Main.numbersInAColumn(1_000_000));

        String expectedMessage = "Введено недопустимое значение";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }


    @Test
    @DisplayName("Тест на ввод числа от 1 до 9")
    void testNumbersInAColumnFromOneToNine() {

        String expectedOutput = "1\n2\n3\n4\n5\n6\n7\n8\n9\n";
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        for (int i = 1; i <= 9; i++) {
            Main.numbersInAColumn(i);
        }

        String actualOutput = outputStream.toString();
        assertEquals(expectedOutput, actualOutput);
    }


    @Test
    @DisplayName("Тест на ввод числа 12")
    void testNumberTwelve() {

        String expectedOutput = "1\n2\n";
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        Main.numbersInAColumn(12);

        String actualOutput = outputStream.toString();
        assertEquals(expectedOutput, actualOutput);
    }


    @Test
    @DisplayName("Тест на ввод числа 321")
    void testForEnteringTheNumberThreeHundredTwentyOne() {

        String expectedOutput = "3\n2\n1\n";
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        Main.numbersInAColumn(321);

        String actualOutput = outputStream.toString();
        assertEquals(expectedOutput, actualOutput);
    }
}