package homework.dz_1_3.task_10;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;


class MainTest {

    Main main = new Main();


    @Test
    @DisplayName("Test that tree is printed for valid input")
    public void testPrintTreeValidInput() {

        // Arrange
        String input = "3\n"; // valid input
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        // Act
        main.printTree();
        // Assert
        Assertions.assertTrue(true); // no exception thrown
    }


    @Test
    @DisplayName("Test that exception is thrown for n <= 2")
    public void testPrintTreeInvalidInputNLessThanThree() {

        String input = "2\n";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        Assertions.assertThrows(IllegalArgumentException.class,
                main::printTree);
    }


    @Test
    @DisplayName("Test that exception is thrown for n >= 10")
    public void testPrintTreeInvalidInputNGreaterThanNine() {


        String input = "10\n";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        Assertions.assertThrows(IllegalArgumentException.class,
                main::printTree);
    }
}
