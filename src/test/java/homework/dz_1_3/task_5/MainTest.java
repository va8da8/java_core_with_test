package homework.dz_1_3.task_5;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class MainTest {


    @Test
    @DisplayName("Тест недопустимого значения ")
    void testIllegalArgumentException() {

        assertThrows(IllegalArgumentException.class,
                () -> new Main().remainderOfDivision(0, 5));
        assertThrows(IllegalArgumentException.class,
                () -> new Main().remainderOfDivision(5, 0));
        assertThrows(IllegalArgumentException.class,
                () -> new Main().remainderOfDivision(10, 5));
        assertThrows(IllegalArgumentException.class,
                () -> new Main().remainderOfDivision(5, 10));
    }


    @Test
    @DisplayName("Тест допустимого значения ")
    void testValidArguments() {

        assertEquals(0, new Main()
                .remainderOfDivision(9, 1));
        assertEquals(2, new Main()
                .remainderOfDivision(8, 3));
        assertEquals(7, new Main()
                .remainderOfDivision(7, 9));
        assertEquals(2, new Main()
                .remainderOfDivision(8, 3));
    }
}