package homework.dz_1_3.task_2;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class MainTest {

    Main main = new Main();


    @Test
    @DisplayName("Тест с допустимыми входными данными")
    void testValidInput() {

        int result = main.sumBetweenNumbers(2, 5);
        assertEquals(14, result,
                "Должен возвращать 14 для ввода (2,5)");
    }


    @Test
    @DisplayName("Тест с недопустимым вводом")
    void testInvalidInput() {

        assertThrows(IllegalArgumentException.class, () ->
                main.sumBetweenNumbers(-2, 5),
                "Следует генерировать" +
                        " IllegalArgumentException для ввода (-2,5)");
        assertThrows(IllegalArgumentException.class, () ->
                        main.sumBetweenNumbers(5, 2),
                "Следует генерировать" +
                        " IllegalArgumentException для ввода (5,2)");
    }


    @Test
    @DisplayName("Тест с идентичными входными данными")
    void testSameInput() {

        int result = main.sumBetweenNumbers(3, 3);
        assertEquals(3, result,
                "Должен возвращать 3 для ввода (3,3)");
    }
}