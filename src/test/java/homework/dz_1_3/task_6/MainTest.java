package homework.dz_1_3.task_6;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;


class MainTest {


    @Test
    @DisplayName("Проверка допустимого ввода: 51")
    void minNumberOfBanknotes_validInput() {

        int input = 51;
        String result = captureOutput(() ->
                Main.minNumberOfBanknotes(input)).trim();
        assertEquals("6 0 1 1", result);
    }


    @Test
    @DisplayName("Тестирование минимального входного значения: 1")
    void minNumberOfBanknotes_minInputValue() {

        int input = 1;
        String result = captureOutput(() ->
                Main.minNumberOfBanknotes(input)).trim();
        assertEquals("0 0 0 1", result);
    }


    @Test
    @DisplayName("Тестирование максимального входного значения: 999_999")
    void minNumberOfBanknotes_maxInputValue() {

        int input = 999_999;
        String result = captureOutput(() ->
                Main.minNumberOfBanknotes(input)).trim();
        assertEquals("124999 1 1 1", result);
    }


    @Test
    @DisplayName("Проверка входного значения меньше 1")
    void minNumberOfBanknotes_inputValueLessThan1() {

        int input = 0;
        IllegalArgumentException exception =
                assertThrows(IllegalArgumentException.class,
                        () -> Main.minNumberOfBanknotes(input));
        assertEquals("Введено недопустимое значение",
                exception.getMessage());
    }


    @Test
    @DisplayName("Проверка входного значения больше 999_999")
    void minNumberOfBanknotes_inputValueGreaterThan999_999() {

        int input = 1_000_000;
        IllegalArgumentException exception =
                assertThrows(IllegalArgumentException.class,
                        () -> Main.minNumberOfBanknotes(input));
        assertEquals("Введено недопустимое значение",
                exception.getMessage());
    }


    // Вспомогательный метод для захвата вывода консоли
    public String captureOutput(Runnable runnable) {

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(outputStream);
        PrintStream ogPrintStream = System.out;
        System.setOut(printStream);

        runnable.run();

        System.out.flush();
        System.setOut(ogPrintStream);
        return outputStream.toString();
    }
}