package homework.dz_1_3.task_3;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class MainTest {

    Main main = new Main();


    @Test
    @DisplayName("Тестирование с корректными значениями")
    void testRootOfNumbers() {

        int actual = main.rootOfNumbers(2, 3);
        int expected = 14;
        assertEquals(expected, actual);
    }


    @Test
    @DisplayName("Тестирование с некорректными значениями")
    void testRootOfNumbersInvalid() {

        assertThrows(IllegalArgumentException.class, () ->
                main.rootOfNumbers(10, 8));
    }
}