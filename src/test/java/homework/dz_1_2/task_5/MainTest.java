package homework.dz_1_2.task_5;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class MainTest {


    @Test
    @DisplayName("Корректный результат при наличии решения")
    void testSolutionExists() {

        Main main = new Main();
        assertEquals("Решение есть",
                main.isThereSolution(1, -95, 18));
        assertEquals("Решение есть",
                main.isThereSolution(46, 44, 3));
        assertEquals("Решение есть",
                main.isThereSolution(31, -89, 4));
    }


    @Test
    @DisplayName("Корректный результат при отсутствии решения")
    void testSolutionDoesNotExist() {

        Main main = new Main();
        assertEquals("Решения нет",
                main.isThereSolution(34, 35, 39));
        assertEquals("Решения нет",
                main.isThereSolution(2, 1, 3));
        assertEquals("Решения нет",
                main.isThereSolution(1, 1, 1));
    }


    @Test
    @DisplayName("Недопустимое значение")
    void testInvalidInput() {

        Main main = new Main();
        assertThrows(IllegalArgumentException.class,
                () -> main.isThereSolution(100, 2, 3));
        assertThrows(IllegalArgumentException.class,
                () -> main.isThereSolution(1, 100, 3));
        assertThrows(IllegalArgumentException.class,
                () -> main.isThereSolution(1, 2, 100));
        assertThrows(IllegalArgumentException.class,
                () -> main.isThereSolution(-100, 2, 3));
        assertThrows(IllegalArgumentException.class,
                () -> main.isThereSolution(1, -100, 3));
        assertThrows(IllegalArgumentException.class,
                () -> main.isThereSolution(1, 2, -100));
    }
}