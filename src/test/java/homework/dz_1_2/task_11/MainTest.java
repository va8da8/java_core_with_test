package homework.dz_1_2.task_11;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class MainTest {

    Main main = new Main();


    @Test
    @DisplayName("Треугольник существует")
    public void isTrueTriangle_true() {

        assertTrue(main.isTrueTriangle(3,4,5));
    }


    @Test
    @DisplayName("Один из параметров ноль")
    public void isTrueTriangle_zero() {

        assertThrows(IllegalArgumentException.class, () ->
            main.isTrueTriangle(0, 5,6));
    }


    @Test
    @DisplayName("Один из параметров отрицательный")
    public void isTrueTriangle_negative() {

        assertThrows(IllegalArgumentException.class, () ->
            main.isTrueTriangle(-3, 4,5));
    }


    @Test
    @DisplayName("Сумма двух меньших сторон не больше третьей")
    public void isTrueTriangle_false() {

        assertFalse(main.isTrueTriangle(2,2,5));
    }
}