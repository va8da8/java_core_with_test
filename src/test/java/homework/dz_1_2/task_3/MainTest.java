package homework.dz_1_2.task_3;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class MainTest {


    @Test
    @DisplayName("Testing lunch method with x = 10")
    void testLunchWithX10() {

        Main main = new Main();
        String result = main.lunch(10);
        assertEquals("Рано", result);
    }


    @Test
    @DisplayName("Testing lunch method with x = 15")
    void testLunchWithX15() {

        Main main = new Main();
        String result = main.lunch(15);
        assertEquals("Пора", result);
    }


    @Test
    @DisplayName("Testing lunch method with x = -1")
    void testLunchWithNegativeX() {

        Main main = new Main();
        assertThrows(IllegalArgumentException.class,
                () -> main.lunch(-1));
    }


    @Test
    @DisplayName("Testing lunch method with x = 24")
    void testLunchWithOutOfRangeX() {

        Main main = new Main();
        assertThrows(IllegalArgumentException.class,
                () -> main.lunch(24));
    }
}