package homework.dz_1_2.task_7;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class MainTest {

    private final Main main = new Main();


    @Test
    @DisplayName("Возвращает корректный результат," +
            " если входная строка состоит из двух слов")
    void testTwoStringsFirstSpaceWithTwoWords() {

        String input = "Hello world";
        String expectedOutput = "Hello\nworld";
        assertEquals(expectedOutput, main.twoStringsFirstSpace(input));
    }


    @Test
    @DisplayName("Возвращает корректный результат," +
            " если входная строка состоит из трех слов")
    void testTwoStringsFirstSpaceWithThreeWords() {

        String input = "Hi great team!";
        String expectedOutput = "Hi\ngreat team!";
        assertEquals(expectedOutput, main.twoStringsFirstSpace(input));
    }


    @Test
    @DisplayName("Выбрасывает IllegalArgumentException," +
            " если входная строка слишком короткая")
    void testTwoStringsFirstSpaceWithShortInputString() {

        String input = "no";
        assertThrows(IllegalArgumentException.class,
                () -> main.twoStringsFirstSpace(input));
    }


    @Test
    @DisplayName("Выбрасывает IllegalArgumentException," +
            " если входная строка слишком длинная")
    void testTwoStringsFirstSpaceWithLongInputString() {

        String input = "hello ".repeat(20);
        assertThrows(IllegalArgumentException.class,
                () -> main.twoStringsFirstSpace(input));
    }


    @Test
    @DisplayName("Выбрасывает IllegalArgumentException," +
            " если входная строка начинается или заканчивается" +
            " на пробел")
    void
    testTwoStringsFirstSpaceWithInputStringStartingOrEndingWithSpace() {

        String input1 = " hello";
        String input2 = "world ";
        assertAll(
                () -> assertThrows(IllegalArgumentException.class,
                        () -> main.twoStringsFirstSpace(input1)),
                () -> assertThrows(IllegalArgumentException.class,
                        () -> main.twoStringsFirstSpace(input2))
        );
    }
}