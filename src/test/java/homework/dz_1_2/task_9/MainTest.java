package homework.dz_1_2.task_9;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class MainTest {

    Main main = new Main();


    @Test
    @DisplayName("Тест при x = 0")
    void testIsTrueIdentityXEqualsZero() {

        assertTrue(main.isTrueIdentity(0));
    }


    @Test
    @DisplayName("Тест при x = 1000")
    void testIsTrueIdentityXEquals1000() {

        assertThrows(IllegalArgumentException.class,
                () -> main.isTrueIdentity(1000));
    }


    @Test
    @DisplayName("Тест при x = -1000")
    void testIsTrueIdentityXEqualsMinus1000() {

        assertThrows(IllegalArgumentException.class,
                () -> main.isTrueIdentity(-1000));
    }
}