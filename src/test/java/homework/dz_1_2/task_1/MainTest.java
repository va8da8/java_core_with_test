package homework.dz_1_2.task_1;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class MainTest {


    @Test
    void testGradeReturnsCorrectResult() {

        Main main = new Main();
        assertEquals("Петя, пора трудиться",
                main.grade(10, 5, 2));
        assertEquals("Петя молодец!",
                main.grade(4, 20, 15));
    }


    @Test
    void testGradeThrowsExceptionForInvalidInput() {

        Main main = new Main();
        assertThrows(IllegalArgumentException.class,
                () -> main.grade(0, 5, 10));
        assertThrows(IllegalArgumentException.class,
                () -> main.grade(2, 100, 10));
        assertThrows(IllegalArgumentException.class,
                () -> main.grade(10, 5, 0));
        assertThrows(IllegalArgumentException.class,
                () -> main.grade(0, 5, 100));
        assertThrows(IllegalArgumentException.class,
                () -> main.grade(100, -1, -1));
    }
}