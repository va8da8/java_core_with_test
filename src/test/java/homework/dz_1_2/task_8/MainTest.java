package homework.dz_1_2.task_8;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class MainTest {

    private final Main main = new Main();


    @Test
    @DisplayName("Возвращает корректный результат," +
            " если входная строка состоит из двух слов")
    void testTwoStringsLasSpaceWithTwoWords() {

        String input = "Hello world";
        String expectedOutput = "Hello\nworld";
        assertEquals(expectedOutput, main.twoStringsLastSpace(input));
    }


    @Test
    @DisplayName("Возвращает корректный результат," +
            " если входная строка состоит из трех слов")
    void testTwoStringsLasSpaceWithThreeWords() {

        String input = "Hi great team!";
        String expectedOutput = "Hi great\nteam!";
        assertEquals(expectedOutput, main.twoStringsLastSpace(input));
    }


    @Test
    @DisplayName("Выбрасывает IllegalArgumentException," +
            " если входная строка слишком короткая")
    void testTwoStringsLasSpaceWithShortInputString() {

        String input = "no";
        assertThrows(IllegalArgumentException.class,
                () -> main.twoStringsLastSpace(input));
    }


    @Test
    @DisplayName("Выбрасывает IllegalArgumentException," +
            " если входная строка слишком длинная")
    void testTwoStringsLasSpaceWithLongInputString() {

        String input = "hello ".repeat(20);
        assertThrows(IllegalArgumentException.class,
                () -> main.twoStringsLastSpace(input));
    }


    @Test
    @DisplayName("Выбрасывает IllegalArgumentException," +
            " если входная строка начинается или заканчивается" +
            " на пробел")
    void
    testTwoStringsLasSpaceWithInputStringStartingOrEndingWithSpace() {

        String input1 = " hello";
        String input2 = "world ";
        assertAll(
                () -> assertThrows(IllegalArgumentException.class,
                        () -> main.twoStringsLastSpace(input1)),
                () -> assertThrows(IllegalArgumentException.class,
                        () -> main.twoStringsLastSpace(input2))
        );
    }
}