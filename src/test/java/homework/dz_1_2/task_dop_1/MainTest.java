package homework.dz_1_2.task_dop_1;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


class MainTest {

    Main main = new Main();


    @Test
    @DisplayName("Проверка, надежный ли пароль")
    void isStrongPassword() {

        String password = "TestPassword123-*_";
        String result = main.isStrongPassword(password);
        Assertions.assertEquals("пароль надежный", result);
    }


    @Test
    @DisplayName("Проверка, не является ли пароль слабым")
    void isWeakPassword() {

        String password = "TestPassword123";
        String result = main.isStrongPassword(password);
        Assertions.assertEquals("пароль не прошел проверку",
                result);
    }
}