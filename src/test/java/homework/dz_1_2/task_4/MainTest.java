package homework.dz_1_2.task_4;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class MainTest {


    @Test
    @DisplayName("Корректное значение дня недели")
    void testValidDayNumber() {

        Main main = new Main();
        assertEquals("5", main.dayNumber(1));
        assertEquals("4", main.dayNumber(2));
        assertEquals("3", main.dayNumber(3));
        assertEquals("2", main.dayNumber(4));
        assertEquals("1", main.dayNumber(5));
        assertEquals("Ура, выходные!", main.dayNumber(6));
        assertEquals("Ура, выходные!", main.dayNumber(7));
    }


    @Test
    @DisplayName("Недопустимое значение дня недели")
    void testInvalidDayNumber() {

        Main main = new Main();
        assertThrows(IllegalArgumentException.class,
                () -> main.dayNumber(0));
        assertThrows(IllegalArgumentException.class,
                () -> main.dayNumber(8));
    }
}