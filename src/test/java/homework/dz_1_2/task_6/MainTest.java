package homework.dz_1_2.task_6;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class MainTest {


    @Test
    @DisplayName("Тест метода englishLevel с недопустимым значением")
    void testEnglishLevelWithInvalidValue() {

        Main main = new Main();
        assertThrows(IllegalArgumentException.class,
                () -> main.englishLevel(0));
    }


    @Test
    @DisplayName("Тест метода englishLevel для уровня beginner")
    void testEnglishLevelBeginner() {

        Main main = new Main();
        assertEquals("beginner", main.englishLevel(499));
    }


    @Test
    @DisplayName("Тест метода englishLevel для уровня pre-intermediate")
    void testEnglishLevelPreIntermediate() {

        Main main = new Main();
        assertEquals("pre-intermediate",
                main.englishLevel(1000));
    }


    @Test
    @DisplayName("Тест метода englishLevel для уровня intermediate")
    void testEnglishLevelIntermediate() {

        Main main = new Main();
        assertEquals("intermediate",
                main.englishLevel(2000));
    }


    @Test
    @DisplayName("Тест метода englishLevel для уровня upper-intermediate")
    void testEnglishLevelUpperIntermediate() {

        Main main = new Main();
        assertEquals("upper-intermediate",
                main.englishLevel(3000));
    }


    @Test
    @DisplayName("Тест метода englishLevel для уровня fluent")
    void testEnglishLevelFluent() {

        Main main = new Main();
        assertEquals("fluent", main.englishLevel(5000));
    }
}