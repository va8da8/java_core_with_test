package homework.dz_1_2.task_10;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


class MainTest {

    Main main = new Main();


    @Test
    @DisplayName("Тестирование с допустимыми входными данными\n")
    void testIsTrueLogarithmicValidInput() {

        assertTrue(main.isTrueLogarithmic(0));
        assertTrue(main.isTrueLogarithmic(1));
        assertTrue(main.isTrueLogarithmic(-1));
        assertTrue(main.isTrueLogarithmic(2.5));
        assertTrue(main.isTrueLogarithmic(-2.5));
    }


    @Test
    @DisplayName("Тестирование с недопустимым вводом")
    void testIsTrueLogarithmicInvalidInput() {

        IllegalArgumentException exception =
                assertThrows(IllegalArgumentException.class, () ->
            main.isTrueLogarithmic(-500));
        assertEquals("Введено недопустимое значение",
                exception.getMessage());

        exception = assertThrows(IllegalArgumentException.class, () ->
                main.isTrueLogarithmic(500));
        assertEquals("Введено недопустимое значение",
                exception.getMessage());
    }
}