package homework.dz_1_2.task_dop_2;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class MainTest {

    Main main = new Main();


    @Test
    @DisplayName("Тест на наличие камней и запрещенной продукции")
    void testWhatInsideStonesAndProhibitedProducts() {

        String mailPackage = "В посылке камни! и запрещенная продукция";
        assertEquals("в посылке камни и запрещенная продукция",
                main.whatInside(mailPackage));
    }


    @Test
    @DisplayName("Тест на наличие запрещенной продукции")
    void testWhatInsideProhibitedProducts() {

        String mailPackage = "В посылке запрещенная продукция";
        assertEquals("в посылке запрещенная продукция",
                main.whatInside(mailPackage));
    }


    @Test
    @DisplayName("Тест на наличие камней")
    void testWhatInsideStones() {

        String mailPackage = "камни! в посылке";
        assertEquals("камни в посылке",
                main.whatInside(mailPackage));
    }


    @Test
    @DisplayName("Тест на отсутствие запрещенной продукции и камней")
    void testWhatInsideNothing() {

        String mailPackage = "Все ок";
        assertEquals("все ок", main.whatInside(mailPackage));
    }
}