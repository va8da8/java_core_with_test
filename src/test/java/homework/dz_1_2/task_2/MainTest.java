package homework.dz_1_2.task_2;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class MainTest {


    @Test
    @DisplayName("isTrueFirstQuadrant returns true for positive x and y")
    void testIsTrueFirstQuadrantReturnsTrueForPositiveXY() {

        Main main = new Main();
        assertTrue(main.isTrueFirstQuadrant(42, 15));
        assertTrue(main.isTrueFirstQuadrant(46, 26));
    }


    @Test
    @DisplayName("isTrueFirstQuadrant returns false for negative x or y")
    void testIsTrueFirstQuadrantReturnsFalseForNegativeXY() {

        Main main = new Main();
        assertFalse(main.isTrueFirstQuadrant(-50, 12));
        assertFalse(main.isTrueFirstQuadrant(0, 23));
        assertFalse(main.isTrueFirstQuadrant(-10, -20));
    }


    @Test
    @DisplayName("isTrueFirstQuadrant throws exception for invalid input")
    void testIsTrueFirstQuadrantThrowsExceptionForInvalidInput() {

        Main main = new Main();
        assertThrows(IllegalArgumentException.class,
                () -> main.isTrueFirstQuadrant(-100, 20));
        assertThrows(IllegalArgumentException.class,
                () -> main.isTrueFirstQuadrant(10, 100));
        assertThrows(IllegalArgumentException.class,
                () -> main.isTrueFirstQuadrant(200, -200));
    }
}