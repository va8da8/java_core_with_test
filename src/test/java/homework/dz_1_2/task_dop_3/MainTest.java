package homework.dz_1_2.task_dop_3;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


class MainTest {

    Main main = new Main();


    @Test
    @DisplayName("Тест должен возвращать «Можно купить», " +
            "если модель «samsung» и цена находится в диапазоне" +
            " от 50 000 до 120 000.")
    void testIsBuyWithValidSamsungModelAndPrice() {

        String result = main.isBuy("samsung galaxy", 80_000);
        assertEquals("Можно купить", result);
    }


    @Test
    @DisplayName("Тест должен возвращать «Можно купить»," +
            " если модель — «iphone», а цена — от 50 000 до 120 000.")
    void testIsBuyWithValidIphoneModelAndPrice() {

        String result = main.isBuy("iphone 12", 95_000);
        assertEquals("Можно купить", result);
    }


    @Test
    @DisplayName("Тест должен возвращать «Не подходит»," +
            " если модель «huawei».")
    void testIsBuyWithInvalidModel() {

        String result = main.isBuy("huawei p30", 70_000);
        assertEquals("Не подходит", result);
    }


    @Test
    @DisplayName("Тест должен возвращать «Не подходит»," +
            " когда цена ниже 50 000")
    void testIsBuyWithInvalidPriceBelow() {

        String result = main.isBuy("samsung galaxy", 40_000);
        assertEquals("Не подходит", result);
    }


    @Test
    @DisplayName("Тест должен возвращать «Не подходит»," +
            " когда цена выше 120 000")
    void testIsBuyWithInvalidPriceAbove() {

        String result = main.isBuy("iphone 12", 130_000);
        assertEquals("Не подходит", result);
    }
}