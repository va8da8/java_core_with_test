package homework.dz_pm_2.task_2;


import java.util.*;


/*
 * С консоли на вход подается две строки s и t. Необходимо вывести true,
 * если одна строка является валидной анаграммой другой строки и false
 * иначе. Анаграмма — это слово или фраза, образованная путем перестановки
 * букв другого слова или фразы, обычно с использованием всех исходных
 * букв ровно один раз.
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        String s = scanner.nextLine();
        String t = scanner.nextLine();

//        String s = "водопад";// "Адрес";
//        String t = "подвода";// "Среда";

        System.out.println(isAnagram(s, t));
    }


    public static boolean isAnagram(String s, String t) {

        // приводим строки к общему регистру
        s = s.toUpperCase();
        t = t.toUpperCase();

        // проверка строк на кол-во символов
        if (s.length() != t.length()) {
            return false;
        }

        // создаем коллекцию символов и заполняем ее разбивая строку на
        // символы
        List<Character> listS = new ArrayList<>();
        for (char ch : s.toCharArray()) {
            listS.add(ch);
        }

        List<Character> listT = new ArrayList<>();
        for (char ch : t.toCharArray()) {
            listT.add(ch);
        }

        // сортируем наши коллекции для их сравнения
        Collections.sort(listS);
        Collections.sort(listT);

        // сравниваем коллекции по значению
        return listS.equals(listT);
    }
}