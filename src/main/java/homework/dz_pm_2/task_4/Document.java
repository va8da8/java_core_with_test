package homework.dz_pm_2.task_4;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/*
 * В некоторой организации хранятся документы (см. класс Document). Сейчас
 * все документы лежат в ArrayList, из-за чего поиск по id документа
 * выполняется неэффективно. Для оптимизации поиска по id, необходимо
 * помочь сотрудникам перевести хранение документов из ArrayList в HashMap.
 *   public class Document {
 *    public int id;
 *    public String name;
 *    public int pageCount;
 *   }
 * Реализовать метод со следующей сигнатурой:
 * public Map<Integer,Document> organizeDocuments(List<Document> documents)
 */
class Document {

    public int id;

    public String name;

    public int pageCount;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public int getPageCount() {

        return pageCount;
    }

    public void setPageCount(int pageCount) {

        this.pageCount = pageCount;
    }


    public Document(int id, String name, int pageCount) {

        this.id = id;
        this.name = name;
        this.pageCount = pageCount;
    }


    public List<Document> documents() {

        List<Document> list = new ArrayList<>();

        list.add(new Document(id, name, pageCount));

        return list;
    }


    public Map<Integer, Document> organizeDocuments
            (List<Document> documents) {

        Map<Integer, Document> map = new HashMap<>();

        for (Document doc : documents) {

            map.put(doc.getId(), doc);
        }
        return map;
    }


    @Override
    public String toString() {

        return id + ", " + name + ", " + pageCount;
    }
}
