package homework.dz_pm_2.task_dop_1;


import java.util.*;


/*
 * Реализовать метод, который принимает массив words и целое положительное
 * число k. Необходимо вернуть k наиболее часто встречающихся слов...
 * Результирующий массив должен быть отсортирован по убыванию частоты
 * встречаемого слова. В случае одинакового количества частоты для слов, то
 * отсортировать и выводить их по убыванию в лексикографическом порядке.
 */
class Main {


    public static void main(String... args) {

        String[] words = {"the", "day", "is", "sunny", "the", "the", "the",
                "sunny", "is", "is", "day"};

        int k = 4;

        System.out.println(Arrays.toString(result(words, k)));
    }


    public static String[] result(String[] words, int k) {

        // создаем карту map TreeMap для сортировки массива в
        // лексикографическом порядке и подсчета повторяющихся элементов
        Map<String, Integer> map = new TreeMap<>();
        for (String value : words) {

            int counter = 1;
            if (map.containsKey(value)) {
                counter = map.get(value);
                counter++;
            }
            map.put(value, counter);
        }

        // создаем коллекцию list для реализации сортировки по значению
        // карты map
        List<Map.Entry<String, Integer>> list =
                new LinkedList(map.entrySet());
        Collections.sort(list, new Comparator<>() {

            @Override
            public int compare(Map.Entry<String, Integer> a,
                               Map.Entry<String, Integer> b) {
                return a.getValue() - b.getValue();
            }
        });

        // создаем карту result LinkedHashMap, для сохранения порядка
        // добавления элементов в карту
        Map<String, Integer> result = new LinkedHashMap<>();
        for (Map.Entry<String, Integer> entry : list) {

            result.put(entry.getKey(), entry.getValue());
        }

        // создаем новую коллекцию LinkedList и заполняем ее ключами
        // карты result
        List<String> listResult = new LinkedList<>(result.keySet());

        // создаем массив для сохранения нужного результата
        String[] wordsResult = new String[k];
        for (int i = 0; i < k; ) {
            // цикл для вывода элементов от большего к меньшему из
            // коллекции listResult
            for (int j = listResult.size() - 1; j >= 0; j--) {
                wordsResult[i] = listResult.get(j);
                i++;
                if (i == k) break;
            }
            break;
        }
        return wordsResult;
    }
}