package homework.dz_pm_2.task_1;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;


/*
 * Реализовать метод, который на вход принимает ArrayList<T>, а возвращает
 * набор уникальных элементов этого массива. Решить используя коллекции.
 */
class Main {


    public static void main(String... args) {

        ArrayList<Integer> arrayList = new ArrayList<>();

        arrayList.add(0);
        arrayList.add(0);
        arrayList.add(1);
        arrayList.add(1);
        arrayList.add(2);

        System.out.println(arrayList);
        System.out.println(uniqueElement(arrayList));


        ArrayList<String> arrayList1 = new ArrayList<>();

        arrayList1.add("Hi");
        arrayList1.add("Hi");
        arrayList1.add("Hello");
        arrayList1.add("Java");
        arrayList1.add("Hello");

        System.out.println(arrayList1);
        System.out.println(uniqueElement(arrayList1));
    }


    public static <T> Set<T> uniqueElement(ArrayList<T> element) {

        return new HashSet<>(element);
    }
}