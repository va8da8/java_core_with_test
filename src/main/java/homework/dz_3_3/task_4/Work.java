package homework.dz_3_3.task_4;


class Work {
    private String nameDog;
    private String nameParticipant;
    private double gpa;


    public Work(String nameDog, String nameParticipant, double grade) {
        this.nameDog = nameDog;
        this.nameParticipant = nameParticipant;
        this.gpa = grade;
    }


    public String getNameDog() {
        return nameDog;
    }

    public void setNameDog(String nameDog) {
        this.nameDog = nameDog;
    }

    public String getNameParticipant() {
        return nameParticipant;
    }

    public void setNameParticipant(String nameParticipant) {

        this.nameParticipant = nameParticipant;
    }

    public double getGpa() {
        return gpa;
    }

    public void setGpa(double gpa) {
        this.gpa = gpa;
    }


    @Override
    public String toString() {

        return nameDog + ":" + nameParticipant + "," + gpa;
    }
}