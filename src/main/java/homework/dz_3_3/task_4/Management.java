package homework.dz_3_3.task_4;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;


class Management {

    Participant participant = new Participant();
    Dog dog = new Dog();
    Grade grade = new Grade();
    private int n;


    Scanner scanner = new Scanner(System.in);


    // заполняем массивы хозяев и собак
    String[] mass() {

        String[] arr = new String[n];
        for (int i = 0; i < n; i++) {

            arr[i] = scanner.nextLine();
        }
        return arr;
    }


    double[] gpa() {

        int[][] arr = new int[n][3];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 3; j++) {

                arr[i][j] = scanner.nextInt();
            }
        }

        // сохраняем средний балл в новый массив
        double[] sum = new double[n];
        for (int i = 0; i < arr.length; i++) {

            int sumInt = 0;
            for (int j = 0; j < arr[i].length; j++) {

                sumInt += arr[i][j];
                // для вывода результата формата (7.6)
                sum[i] = (int) ((sumInt / 3.0) * 10) / 10.0;
            }
        }
        return sum;
    }


    void e() {

        n = 4;

        participant.setFirstName(mass());
        dog.setName(mass());
        grade.setGrade(gpa());

        ArrayList<Work> list = new ArrayList<>();
        for (int i = 0; i < n; i++) {

            list.add(new Work(participant.getFirstName()[i],
                    dog.getName()[i], grade.getGrade()[i]));
        }

        list.sort((o1, o2) -> (int) (o1.getGpa() - o2.getGpa()));
        Collections.reverse(list);
        for (int i = 0; i < 3; i++) {

            System.out.println(list.get(i));
        }
    }
}