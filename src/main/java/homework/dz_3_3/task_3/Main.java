package homework.dz_3_3.task_3;

import java.util.ArrayList;
import java.util.Scanner;


/*
 * На вход передается N — количество столбцов в двумерном массиве и
 * M — количество строк. Необходимо вывести матрицу на экран, каждый
 * элемент которого состоит из суммы индекса столбца и строки этого же
 * элемента. Решить необходимо используя ArrayList.
 */
class Main {


    public static void main(String... args) {

        int n, m;

        Scanner scanner = new Scanner(System.in);

        // количество столбцов в двумерном массиве
        n = scanner.nextInt();

        //количество строк в двумерном массиве
        m = scanner.nextInt();

        ArrayList<ArrayList<Integer>> matrix = new ArrayList<>();

        for (int i = 0; i < m; i++) {
            ArrayList<Integer> list = new ArrayList<>();
            for (int j = 0; j < n; j++) {

                list.add(i + j);
            }
            matrix.add(list);
        }

        for (ArrayList<Integer> lst : matrix) {
            for (Integer i : lst) {

                System.out.print(i + " ");
            }
            System.out.println();
        }

//        ArrayList<int[][]> list = new ArrayList<>();
//
//        int[][] arr = new int[m][n];
//        for (int i = 0; i < m; i++) {
//            for (int j = 0; j < n; j++) {
//                arr[i][j] = i + j;
//            }
//        }
//        list.add(arr);
//
//        for (int[][] lst : list) {
//            for (int[] i : lst) {
//                System.out.println(Arrays.toString(i));
//            }
//        }

//        ArrayList<Integer> list = new ArrayList<>();
//
//        for (int i = 0; i < m; i++) {
//            for (int j = 0; j < n; j++) {
//
//                list.add(i + j);
//            }
//        }
//
//        int count = 0;
//        for (Integer el : list){
//
//            if(count == n){
//                System.out.println();
//                count = 0;
//            }
//            System.out.print(el + " ");
//            count++;
//        }
    }
}