package homework.dz_3_3.task_1;


/**
 * Класс Bat, который содержит и реализует два метода из класса родителя
 * Mammal, final movement() без возможности переопределения в классах
 * наследниках и переопределенный метод movementSpeed() с возможностью
 * дальнейшего переопределения.
 */
class Bat extends Mammal {


    @Override
    final void movement() {

        System.out.println("я летаю");
    }


    @Override
    void movementSpeed() {

        System.out.println("моя скорость 10к/ч");
    }
}