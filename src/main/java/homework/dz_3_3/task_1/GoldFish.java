package homework.dz_3_3.task_1;


/**
 * Класс GoldFish, который переопределяет метод movementSpeed() из
 * родителя Fish, с возможностью дальнейшего переопределения потомками.
 */
class GoldFish extends Fish {


    @Override
    void movementSpeed() {

        System.out.println("моя скорость 10 км/ч");
    }
}