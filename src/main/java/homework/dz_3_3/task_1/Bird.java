package homework.dz_3_3.task_1;


/**
 * Абстрактный класс Bird, который содержит и два метода final
 * wayOfBirth() и movement() переопределенные из класса родителя
 * Animals и запрещает дальнейшее переопределение их наследниками,
 * так же класс содержит abstract метод movementSpeed() для переопределения
 * классами наследниками.
 */
abstract class Bird extends Animals {


    @Override
    final void wayOfBirth() {

        System.out.println("откладывают яйца");
    }


    @Override
    final void movement() {

        System.out.println("я летаю");
    }


    abstract void movementSpeed();
}