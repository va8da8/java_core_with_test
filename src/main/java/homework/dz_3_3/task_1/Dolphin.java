package homework.dz_3_3.task_1;


/**
 * Класс Dolphin, который содержит и реализует два метода из класса
 * родителя Mammal, final movement() без возможности переопределения в
 * классах наследниках и переопределенный метод movementSpeed() с
 * возможностью дальнейшего переопределения.
 */
class Dolphin extends Mammal {


    @Override
    final void movement() {

        System.out.println("я плыву");
    }


    @Override
    void movementSpeed() {

        System.out.println("моя скорость 30 км/ч");
    }
}