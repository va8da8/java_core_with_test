package homework.dz_3_3.task_1;


/**
 * Класс Eagle, который переопределяет метод movementSpeed() из родителя
 * Bird, с возможностью дальнейшего переопределения потомками.
 */
class Eagle extends Bird {


    @Override
    void movementSpeed() {

        System.out.println("моя скорость 30 км/ч");
    }
}