package homework.dz_3_3.task_1;


/**
 * Абстрактный класс Mammal, который содержит final метод wayOfBirth()
 * переопределенный из класса родителя Animals и запрещает дальнейшее
 * переопределение его наследниками. Переопределенный abstract метод
 * movement() не имеет реализацию, т.к. должен быть переопределен
 * наследниками как еще один abstract метод movementSpeed().
 */
abstract class Mammal extends Animals {


    @Override
    final void wayOfBirth() {

        System.out.println("живородящие");
    }


    @Override
    abstract void movement();


    abstract void movementSpeed();
}