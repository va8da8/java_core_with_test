package homework.dz_3_3.task_1;


/**
 * Абстрактный класс Fish, который содержит и два метода final
 * wayOfBirth() и movement() переопределенные из класса родителя
 * Animals и запрещает дальнейшее переопределение их наследниками,
 * так же класс содержит abstract метод movementSpeed() для переопределения
 * классами наследниками.
 */
abstract class Fish extends Animals {


    @Override
    final void wayOfBirth() {

        System.out.println("мечут икру");
    }


    @Override
    final void movement() {

        System.out.println("я плыву");
    }


    abstract void movementSpeed();
}