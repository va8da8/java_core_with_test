package homework.dz_3_3.task_2;


/*
 * Цех по ремонту BestCarpenterEver умеет чинить некоторую Мебель. К
 * сожалению, из Мебели он умеет чинить только Табуретки, а Столы,
 * например,
 * нет. Реализовать метод в цеху, позволяющий по переданной мебели
 * определять, сможет ли ей починить или нет. Возвращать результат типа
 * boolean. Протестировать метод.
 */

/**
 * Класс BestCarpenterEver, который содержит boolean метод isTakeToWork()
 * принимающий на вход название предмета для последующего ремонта, если
 * цех умеет чинить этот предмет то вернется true, иначе false. Также
 * присутствует метод main(), для создания объекта класса и вывода
 * результата проверки метода isTakeToWork().
 */
class BestCarpenterEver {


    boolean isTakeToWork(String furnitureName) {

        return furnitureName.equals("табуретка");
    }


    public static void main(String... args) {

        BestCarpenterEver b = new BestCarpenterEver();
        BestCarpenterEver b1 = new BestCarpenterEver();
        System.out.println(b.isTakeToWork("табуретка"));
        System.out.println(b1.isTakeToWork("стол"));
    }
}