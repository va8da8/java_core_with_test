package homework.dz_2_2.task_7;


import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;


/*
 * Раз в год Петя проводит конкурс красоты для собак. К сожалению,
 * система хранения участников и оценок неудобная, а победителя
 * определить надо. В первой таблице в системе хранятся имена хозяев,
 * во второй - клички животных, в третьей — оценки трех судей за
 * выступление каждой собаки. Таблицы связаны между собой только
 * по индексу. То есть хозяин i-ой собаки указан в i-ой строке
 * первой таблицы, а ее оценки — в i-ой строке третьей таблицы.
 * Нужно помочь Пете определить топ 3 победителей конкурса.
 * На вход подается число N — количество участников конкурса.
 * Затем в N строках переданы имена хозяев. После этого в N
 * строках переданы клички собак. Затем передается матрица с
 * N строк, 3 вещественных числа в каждой — оценки судей.
 * Победителями являются три участника, набравшие максимальное
 * среднее арифметическое по оценкам 3 судей. Необходимо вывести
 * трех победителей в формате “Имя хозяина: кличка, средняя оценка”.
 * Гарантируется, что среднее арифметическое для всех участников
 * будет различным.
 */
class Main {


    public static void main(String... args) {

        start();
    }


    /**
     * Метод start(), который не принимает значения на вход. В нем
     * происходит заполнения массивов "участники", "клички собак",
     * "оценки судей". Так же происходит вызов метода
     * res(participants, dogNames, averageMark(rating)).
     */
    public static void start() {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        scanner.nextLine();
        int numberOfJudges = 3;

        String[] participants = new String[n];
        // заполняем массив "участников"
        for (int i = 0; i < participants.length; i++) {

            participants[i] = scanner.nextLine();
        }

        String[] dogNames = new String[n];
        // заполняем массив "клички собак"
        for (int i = 0; i < dogNames.length; i++) {

            dogNames[i] = scanner.nextLine();
        }

        int[][] rating = new int[n][numberOfJudges];
        // заполняем двухмерный массив "оценки судей"
        for (int i = 0; i < rating.length; i++) {
            for (int j = 0; j < rating[i].length; j++) {

                rating[i][j] = scanner.nextInt();
            }
        }

        res(participants, dogNames, averageMark(rating));
    }


    /**
     * Метод double[] averageMark, который принимает на вход
     * двухмерный массив int[][] rating "оценки судей", а
     * возвращает одномерный массив double[] gpa "средняя оценка".
     */
    public static double[] averageMark(int[][] rating) {

        double[] gpa = new double[rating.length];

        for (int i = 0; i < rating.length; i++) {
            for (int j = 0; j < rating[i].length; j++) {

                gpa[i] += rating[i][j];
            }
            // для вывода значений типа double в виде 7.6
            gpa[i] = ((int)
                    ((gpa[i] / rating[0].length) * 10) / 10.0);
        }

        return gpa;
    }


    /**
     * Метод res, который принимает на вход три массива "участники",
     * "клички собак", "средняя оценка". Создается двухмерный массив
     * и заполняется последовательно значениями входных массивов.
     * Для вывода участников награждения, происходит сортировка по
     * среднему баллу и дальнейший вывод значений.
     */
    public static void res(String[] participants, String[] dogNames,
                           double[] averageMark) {

        int outputValues = 3;
        int participantsForTheAward = 3;

        String[][] res =
                new String[participants.length][outputValues];

        for (int i = 0; i < participants.length; i++) {
            for (int j = 0; j < outputValues; j++) {

                res[i][j] = participants[i] + ": ";
                j++;
                res[i][j] = dogNames[i] + ", ";
                j++;
                res[i][j] = String.valueOf(averageMark[i]);
            }

        }

        Arrays.sort(res, Comparator.comparing(o -> o[2]));

        for (int i = participants.length - 1; i > 0; i--) {
            for (int j = 0; j < participantsForTheAward; j++) {

                System.out.print(res[i][j]);
            }
            System.out.println();
        }
    }
}