package homework.dz_2_2.task_3;


import java.util.Scanner;


/*
 * На вход подается число N — количество строк и столбцов матрицы.
 * Затем передаются координаты X и Y расположения коня на шахматной
 * доске.
 * Необходимо заполнить матрицу размера NxN нулями, местоположение
 * коня отметить символом K, а позиции, которые он может бить,
 * символом X.
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        int x_1 = scanner.nextInt();
        int y_1 = scanner.nextInt();

        char[][] arr = new char[n][n];

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {

                arr[i][j] = '0';

                if (i == x_1 && j == y_1) {
                    arr[i][j] = 'K';
                }


                if (((i == x_1 - 2 || i == x_1 + 2) &&
                        (j == y_1 - 1 || j == y_1 + 1)) ||
                        ((i == x_1 - 1 || i == x_1 + 1) &&
                                (j == y_1 - 2 || j == y_1 + 2))) {
                    arr[i][j] = 'X';
                }


//                int xX = Math.abs(x_1 - i);
//                int yX = Math.abs(y_1 - j);
//                if ((xX == 1 && yX == 2) || (xX == 2 && yX == 1)) {
//
//                    arr[i][j] = 'X';
//                }
            }
        }

        for (char[] ints : arr) {
            for (int j = 0; j < ints.length; j++) {

                // для проверки ДЗ ботом, убираем пробелы
                // с последнего столбца
                if (j != n - 1) System.out.print(ints[j] + " ");
                else System.out.print(ints[j]);
            }
            System.out.println();
        }
    }
}