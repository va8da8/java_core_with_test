package homework.dz_2_2.task_1;


import java.util.Scanner;


/*
 * На вход передается N — количество столбцов в двумерном массиве и
 * M — количество строк. Затем сам передается двумерный массив,
 * состоящий из натуральных чисел.
 * Необходимо сохранить в одномерном массиве и вывести на экран
 * минимальный элемент каждой строки.
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int m = scanner.nextInt();

        int[][] arr = new int[m][n];

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {

                arr[i][j] = scanner.nextInt();
            }
        }

        minElementOfAString(arr, m);
    }


    public static void minElementOfAString(int[][] arr, int m) {

        int[] mass = new int[m];

        for (int i = 0; i < arr.length; i++) {

            int min = arr[i][0];
            //int max = arr[i][0];

            for (int j = 0; j < arr[0].length; j++) {

                if (min > arr[i][j]) {
                    min = arr[i][j];
                }
//                if(max < arr[i][j]){
//                    max = arr[i][j];
//
//                }
            }
            mass[i] = min;
        }

        for (int j : mass) {

            System.out.print(j + " ");
        }
    }
}