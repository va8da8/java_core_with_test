package homework.dz_2_2.task_5;


import java.util.Scanner;


/*
 * На вход подается число N — количество строк и столбцов матрицы.
 * Затем передается сама матрица, состоящая из натуральных чисел.
 * Необходимо вывести true, если она является симметричной
 * относительно побочной диагонали, false иначе.
 * Побочной диагональю называется диагональ, проходящая из
 * верхнего правого угла в левый нижний.
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        int[][] arr = new int[n][n];

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {

                arr[i][j] = scanner.nextInt();
            }
        }

        System.out.println(isSymmetrical(arr));
    }


    public static boolean isSymmetrical(int[][] arr) {

        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - i - 1; j++) {

                if (arr[i][j] ==
                        arr[arr.length - j - 1][arr.length - i - 1]) {
                    return true;
                }
            }
        }
        return false;
    }
}