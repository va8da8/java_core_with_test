package homework.dz_2_2.task_6;


import java.util.Scanner;


/*
 * Петя решил начать следить за своей фигурой. Но все существующие
 * приложения для подсчета калорий ему не понравились и он решил
 * написать свое. Петя хочет каждый день записывать сколько белков,
 * жиров, углеводов и калорий он съел, а в конце недели приложение
 * должно его уведомлять, вписался ли он в свою норму или нет.
 * На вход подаются числа A — недельная норма белков, B — недельная
 * норма жиров, C — недельная норма углеводов и K — недельная норма
 * калорий. Затем передаются 7 строк, в которых в том же порядке
 * указаны сколько было съедено Петей нутриентов в каждый день
 * недели. Если за неделю в сумме по каждому нутриенту не
 * превышена недельная норма, то вывести “Отлично”,
 * иначе вывести “Нужно есть поменьше”.
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int[] weeklyRate = new int[4];

        for (int i = 0; i < weeklyRate.length; i++) {

            weeklyRate[i] = scanner.nextInt();
        }

        int[][] arr = new int[7][weeklyRate.length];

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {

                arr[i][j] = scanner.nextInt();
            }
        }

        comparisonArrays(sumOfColumns(arr), weeklyRate);
    }


    /**
     * Метод int[] sumOfColumns, который принимает на вход
     * двухмерный массив типа int для подсчета в нем суммы столбцов.
     * Записывает результат в созданный массив и возвращает его.
     */
    public static int[] sumOfColumns(int[][] arr) {

        int[] mass = new int[arr[0].length];

        for (int i = 0; i < arr[0].length; i++) {
            for (int[] ints : arr) {

                mass[i] += ints[i];
            }
        }
        return mass;
    }


    /**
     * Метод void comparisonArrays, который принимает на вход
     * два массива типа int и проверяет, не являются ли значения
     * одного больше другого. Выводит соответсвующий результат.
     */
    public static void comparisonArrays(int[] mass, int[] arr) {

        int count = 0;

        for (int i = 0; i < mass.length; i++) {

            if (mass[i] <= arr[i]) count++;
            else count--;
        }

        if (count == mass.length) System.out.println("Отлично");
        else System.out.println("Нужно есть поменьше");
    }
}