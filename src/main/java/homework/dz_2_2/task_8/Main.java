package homework.dz_2_2.task_8;


import java.util.Scanner;


/*
 * На вход подается число N. Необходимо посчитать и вывести на
 * экран сумму его цифр. Решить задачу нужно через рекурсию.
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        System.out.println(sumNumbers(n));
    }


    public static int sumNumbers(int n) {

        if (n < 10) {

            return n;
        }
        return (n % 10) + sumNumbers(n / 10);
    }
}