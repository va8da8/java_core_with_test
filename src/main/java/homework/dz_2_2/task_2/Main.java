package homework.dz_2_2.task_2;


import java.util.Scanner;


/*
 * На вход подается число N — количество строк и столбцов матрицы. Затем
 * в последующих двух строках подаются координаты X (номер столбца)
 * и Y (номер строки) точек, которые задают прямоугольник.
 * Необходимо отобразить прямоугольник с помощью символа 1 в матрице,
 * заполненной нулями (см. пример) и вывести всю матрицу на экран.
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int x_1 = scanner.nextInt();
        int y_1 = scanner.nextInt();
        int x_2 = scanner.nextInt();
        int y_2 = scanner.nextInt();

        int[][] arr = new int[n][n];

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {

                arr[i][j] = 0;

                if (i == y_1 && j >= x_1 && j <= x_2) {
                    arr[i][j] = 1;
                    continue;
                }
                if (i >= y_1 && i <= y_2 && j == x_1) {
                    arr[i][j] = 1;
                    continue;
                }
                if (i == y_2 && j >= x_1 && j <= x_2) {
                    arr[i][j] = 1;
                    continue;
                }
                if (i <= y_2 && i >= y_1 && j == x_2) {
                    arr[i][j] = 1;
                }
            }
        }

        for (int[] ints : arr) {
            for (int j = 0; j < ints.length; j++) {

                // для проверки ДЗ ботом, убираем пробелы
                // из последнего столбца
                if (j != n - 1) System.out.print(ints[j] + " ");
                else System.out.print(ints[j]);
            }
            System.out.println();
        }
    }
}