package homework.dz_2_2.task_9;


import java.util.Scanner;


/*
 * На вход подается число N. Необходимо вывести цифры числа слева
 * направо. Решить задачу нужно через рекурсию.
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        numbersFromLeftToRight(n);
    }


    public static void numbersFromLeftToRight(int n) {

        if (n > 0) {

            int numeric = 0;
            int number = n % 10;
            numeric += number;
            number = n / 10;
            numbersFromLeftToRight(number);
            System.out.print(numeric + " ");
        }

//        if (n < 10) {
//            System.out.print(n);
//            return;
//        }
//        numbersFromLeftToRight(n / 10);
//        System.out.print(" " + n % 10);
    }
}