package homework.dz_2_2.task_10;


import java.util.Scanner;


/*
 * На вход подается число N. Необходимо вывести цифры числа справа
 * налево. Решить задачу нужно через рекурсию.
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        numbersFromRightToLeft(n);
    }


    public static void numbersFromRightToLeft(int n) {

        if (n > 0) {
            int number = n % 10;
            System.out.print(number + " ");
            number = n / 10;
            numbersFromRightToLeft(number);
        }
    }
}