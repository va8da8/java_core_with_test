package homework.dz_2_2.task_4;


import java.util.Scanner;


/*
 * На вход подается число N — количество строк и столбцов матрицы.
 * Затем передается сама матрица, состоящая из натуральных чисел.
 * После этого передается натуральное число P.
 * Необходимо найти элемент P в матрице и удалить столбец и строку
 * его содержащий (т.е. сохранить и вывести на экран массив меньшей
 * размерности). Гарантируется, что искомый элемент единственный
 * в массиве.
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        int[][] arr = new int[n][n];

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {

                arr[i][j] = scanner.nextInt();
            }
        }

        int p = scanner.nextInt();

        dropRowAndColumn(arr, p);
    }


    public static void dropRowAndColumn(int[][] arr, int p) {

        int[][] mass = new int[arr.length - 1][arr.length - 1];
        int row = 0;
        int col = 0;
        int row_mass = 0;
        int col_mass = 0;

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (p == arr[i][j]) {
                    row = i;
                    col = j;
                }
            }
        }

        for (int i = 0; i < arr.length; i++) {
            if (i != row) {

                for (int j = 0; j < arr[i].length; j++) {

                    if (j != col) {
                        mass[row_mass][col_mass] = arr[i][j];
                        col_mass++;
                    }
                }
                col_mass = 0;
                row_mass++;
            }
        }

        for (int[] ints : mass) {
            for (int j = 0; j < ints.length; j++) {

                // для проверки ДЗ ботом, убираем пробелы
                // с последнего столбца
                if (j != mass.length - 1)
                    System.out.print(ints[j] + " ");
                else
                    System.out.print(ints[j]);
            }
            System.out.println();
        }
    }
}