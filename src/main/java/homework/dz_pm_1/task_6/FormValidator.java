package homework.dz_pm_1.task_6;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


class FormValidator {


    // длина имени должна быть от 2 до 20 символов, первая буква заглавная
    public static void checkName(String str) {

        Pattern pattern = Pattern.compile("^[A-Z][a-z]{1,19}$");
        Matcher matcher = pattern.matcher(str);

        try {

            if (!matcher.matches()) {
                throw new CheckException("длина имени должна быть" +
                        " от 2 до 20 символов, первая буква заглавная");
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

//        try {
//            if (!str.matches("^[A-Z]{1}[a-z]{1,19}$")) {
//                throw new CheckNameException("длина имени должна быть" +
//                        " от 2 до 20 символов, первая буква заглавная");
//            }
//        } catch (Exception e) {
//            System.err.println(e.getMessage());
//            return;
//        }
    }


    // дата рождения должна быть не раньше 01.01.1900 и не позже
    // текущей даты.
    public static void checkBirthdate(String str) {

        // создаем текущую дату
        Date date = new Date();

        String strDate = "01.01.1900";
        // приводим текущую дату к нужному формату
        String strDate2 = String.format("%1$td.%1$tm.%1$tY", date);

        // формат даты для парсинга
        SimpleDateFormat format =
                new SimpleDateFormat("dd.MM.yyyy");

        try {

            // парсим из строки дату в формат Date
            Date dates = format.parse(strDate);
            Date dates1 = format.parse(str);
            Date dates2 = format.parse(strDate2);

            if ((dates1.before(dates)) || (dates1.after(dates2))) {
                throw new CheckException("дата рождения должна быть не " +
                        "раньше 01.01.1900 и не позже текущей даты.");
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }


    // пол должен корректно матчится в enum Gender, хранящий
    // Male и Female значения.
    public static void checkGender(String str) {

        String inputString = str.toUpperCase();
        String male = "MALE";
        String female = "FEMALE";

        try {

            if (!(inputString.equals(male) ||
                    inputString.equals(female))) {
                throw new CheckException("введенное значение должно " +
                        "быть Male или Female");
            }
            // вызов метода valueOf(), может вызвать исключение
            // IllegalArgumentException, поэтому мы ставим его после
            // проверки на введенное значение пользователем
            Gender en = Gender.valueOf(inputString);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }


    // рост должен быть положительным числом и корректно конвертироваться
    // в double.
    public static void checkHeight(String str) {
        try {

            double height = Double.parseDouble(str);
            if (height <= 0) {
                throw new CheckException
                        ("рост должен быть положительным числом");
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}