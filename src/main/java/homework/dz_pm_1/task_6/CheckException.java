package homework.dz_pm_1.task_6;


/**
 * Класс CheckException, который наследуется от класса проверяемых
 * исключений Exception. Содержит конструктор для вывода сообщения ошибки.
 */
class CheckException extends Exception {


    public CheckException(String message) {
        super(message);
    }
}