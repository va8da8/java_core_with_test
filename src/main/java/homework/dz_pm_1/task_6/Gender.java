package homework.dz_pm_1.task_6;

/**
 * Финальный класс Gender, который содержит финальные поля MALE, FEMALE.
 */
enum Gender {MALE, FEMALE}