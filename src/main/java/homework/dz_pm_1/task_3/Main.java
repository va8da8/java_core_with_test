package homework.dz_pm_1.task_3;


import java.io.*;


/*
 * Реализовать метод, открывающий файл ./input.txt и сохраняющий в файл
 * ./output.txt текст из input, где каждый латинский строчный символ
 * заменен на соответствующий заглавный. Обязательно использование
 * try с ресурсами.
 */
class Main {


    /**
     * Метод main(String... args), который запускает программу и вызывает
     * два статических метода writeFileInput() и overwriteFileOutput().
     */
    public static void main(String... args) {

        writeFileInput();
        overwriteFileOutput();
    }


    /**
     * Метод writeFileInput(), создает и записывает в файл с помощью
     * класса FileOutputStream используя конструктор с параметром
     * (путь к файлу). Записывает латинский алфавит строчными буквами
     * используя массив byte.
     */
    static void writeFileInput() {

        try (FileOutputStream fos = new FileOutputStream
                ("input.txt")) {

            char ch = 'a';
            byte[] buffer = new byte[26];

            for (int i = 0; i <= buffer.length - 1; i++) {

                buffer[i] = (byte) ch;
                ch++;
                fos.write(buffer[i]);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


    /**
     * Метод overwriteFileOutput(), который читает файл с помощью класса
     * FileInputStream используя конструктор с параметром(путь к файлу).
     * Считывается каждый отдельный байт в переменную i и происходит запись
     * в новый файл с помощью класса FileOutputStream используя конструктор
     * с параметрами (путь к файлу и параметром true,
     * для добавления в файл) и изменением результата строчная
     * буква на прописную.
     */
    static void overwriteFileOutput() {

        try (FileInputStream fin =
                     new FileInputStream("input.txt")) {

            int i;
            while ((i = fin.read()) != -1) {

                // для добавления в файл используем во втором
                // параметре true
                try (FileOutputStream fos = new FileOutputStream
                        ("output.txt", true)) {

                    fos.write((char) (i - 32));
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}