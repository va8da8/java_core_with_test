package homework.dz_pm_1.task_5;


import java.util.Scanner;


/*
 * Найти и исправить ошибки в следующем коде (сдать исправленный вариант):
 * public class Main {
 *  public static void main(String[] args) {
 *   int n = inputN();
 *   System.out.println("Успешный ввод!");
 *  }
 *  private static int inputN() {
 *   System.out.println("Введите число n, 0 < n < 100");
 *   Scanner scanner = new Scanner(System.in);
 *   int n = scanner.nextInt();
 *   if (n < 100 && n > 0) {
 *    throw new Exception("Неверный ввод");
 *   }
 *   return n;
 *  }
 * }
 */
class Main {


    public static void main(String... args) {

        // Я убрал переменную n и изменил метод на void, т.к. данная
        // ситуация не требует возвращаемого значения и переменная n
        // является лишней.
        inputN();
        System.out.println("Успешный ввод!");
    }


    private static void inputN() {

        System.out.println("Введите число n, 0 < n < 100");

        Scanner scanner = new Scanner(System.in);

        int n;

        do {

            n = scanner.nextInt();
            if (!(n < 100 && n > 0)) {

                try {

                    throw new Exception("Неверный ввод");
                } catch (Exception e) {
                    // в зависимости от того, что мы хотим вывести
                    // указываем в этом блоке
                    //e.printStackTrace();
                    //System.out.println(e);
                    System.out.println(e.getMessage());
                }
            }
        } while (!(n < 100 && n > 0));
    }
}