package homework.dz_pm_1.task_2;


import java.io.PrintStream;


/*
 * Создать собственное исключение MyUncheckedException, являющееся
 * непроверяемым.
 */

/**
 * Класс MyUncheckedException, который наследуется от класса непроверяемых
 * исключений RuntimeException. Содержит два конструктора для создания
 * исключения с сообщением пользователя MyUncheckedException
 * (String message)
 * и MyUncheckedException(String message, Throwable cause) с сообщением
 * пользователя и выводом исключения породившее исключение пользователя.
 * Содержит два переопределенных метода
 * getCause() - возвращает исключение, которое стало причиной
 * текущего или
 * null, если такого нет и метод printStackTrace(PrintStream s)
 * - для вывода
 * исключения в формате (сообщение исключения пользователя и
 * вывод исключения
 * послужившее причиной или null).
 */
class MyUncheckedException extends RuntimeException {


    public MyUncheckedException(String message) {

        super(message);
    }


    public MyUncheckedException(String message, Throwable cause) {

        super(message, cause);
    }


    @Override
    public synchronized Throwable getCause() {

        return super.getCause();
    }

    @Override
    public void printStackTrace(PrintStream s) {

        System.err.println(getMessage() + ", " + getCause());
    }
}