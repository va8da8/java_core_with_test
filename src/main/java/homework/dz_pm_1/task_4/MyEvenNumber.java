package homework.dz_pm_1.task_4;


/*
 * Создать класс MyEvenNumber, который хранит четное число int n. Используя
 * исключения, запретить создание инстанса с нечетным числом.
 */

/**
 * Класс MyEvenNumber, который содержит поле private int n, два
 * конструктора, один без параметров, другой принимает содержащие поле,
 * методы get()- и set(int n)-. Так же класс содержит метод
 * evenNumber(int n) для проверки числа, является оно четным или нет
 * (выводит ошибку, если нет).
 */
class MyEvenNumber {

    private int n;


    public MyEvenNumber() {}


    public MyEvenNumber(int n) { evenNumber(n); }


    void evenNumber(int n) {

        try {

            if (n % 2 != 0) {
                throw new EvenNumberException("Вы ввели нечетное число");
            }
            this.n = n;
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }


    public void setN(int n) { evenNumber(n); }

    public int getN() { return n; }
}