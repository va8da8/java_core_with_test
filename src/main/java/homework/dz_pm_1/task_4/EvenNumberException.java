package homework.dz_pm_1.task_4;


/**
 * Класс EvenNumberException, который наследуется от класса проверяемых
 * исключений Exception. Содержит конструктор
 * EvenNumberException(String message), который выводит название класса
 * ошибки и сообщение пользователя об ошибке.
 */
class EvenNumberException extends Exception {


    public EvenNumberException(String message) {
        super(message);
        System.err.print("Exception EvenNumberException: ");
    }
}