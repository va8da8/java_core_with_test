package homework.dz_pm_1.task_dop_1;


import java.util.Scanner;


/*
 * На вход подается число n и массив целых чисел длины n. Вывести два
 * максимальных числа в этой последовательности.
 * Пояснение: Вторым максимальным числом считается тот, который окажется
 * максимальным после вычеркивания первого максимума.
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int n;

        do {

            System.out.println("Введите длину массива больше 3");
            n = scanner.nextInt();
        } while (n < 3);

        // создаем массив и заполняем его
        int[] arr = new int[n];
        for (int i = 0; i < arr.length; i++) {

            arr[i] = scanner.nextInt();
        }

        // пузырьковая сортировка массива
        for (int i = 1; i < arr.length; i++) {
            for (int j = arr.length - 1; j >= i; j--) {

                int temp;
                if (arr[j - 1] > arr[j]) {
                    temp = arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = temp;
                }
            }
        }

        // выводим два максимальных числа
        for (int i = arr.length - 1; i > arr.length - 3; i--) {

            System.out.print(arr[i] + " ");
        }
    }
}