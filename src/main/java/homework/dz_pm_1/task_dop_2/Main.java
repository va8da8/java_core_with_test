package homework.dz_pm_1.task_dop_2;


import java.util.Scanner;


/*
 * На вход подается число n, массив целых чисел отсортированных по
 * возрастанию длины n и число p. Необходимо найти индекс элемента массива
 * равного p. Все числа в массиве уникальны. Если искомый элемент не
 * найден, вывести -1.
 * Решить задачу за логарифмическую сложность.
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        int[] arr = new int[n];
        for (int i = 0; i < arr.length; i++) {

            arr[i] = scanner.nextInt();
        }

        int p = scanner.nextInt();

        System.out.println(binarySearch(arr, p));
    }


    private static int binarySearch(int[] arr, int valueToFind) {

        int index = -1;
        int low = 0;
        int high = arr.length - 1;

        while (low <= high) {

            int mid = (low + high) / 2;
            if (arr[mid] < valueToFind) {

                low = mid + 1;
            } else if (arr[mid] > valueToFind) {

                high = mid - 1;
            } else if (arr[mid] == valueToFind) {

                index = mid;
                break;
            }
        }
        return index;
    }
}