package homework.dz_pm_1.task_1;


/*
 * Создать собственное исключение MyCheckedException, являющееся
 *  проверяемым.
 */

/**
 * Класс MyCheckedException, который наследуется от класса проверяемых
 * исключений Exception. Имеет одно private final поле типа String для
 * использования этого значения в конструкторе
 * MyCheckedException(String myText) при создании объекта исключения и
 * метод public void getMyText() для вывода этого значения.
 */
class MyCheckedException extends Exception {

    // добавляем свой текст
    private final String myText;


    // конструктор для создания текста
    public MyCheckedException(String myText) {

        this.myText = myText;
        System.err.print("Exception MyCheckedException: ");
    }


    // через метод берем этот текст
    public void getMyText() { System.err.println(this.myText); }
}