-- заполняем таблицу товар
insert into product (name, price)
values ('Розы', 100);
insert into product (name, price)
values ('Лилии', 50);
insert into product (name, price)
values ('Ромашки', 25);


-- заполняем таблицу покупатель
insert into customer (first_name, phone_number)
values ('Петя', '8-888-888-88-88');
insert into customer (first_name, phone_number)
values ('Вася', '7-777-777-77-77');
insert into customer (first_name, phone_number)
values ('Катя', '1-111-111-11-11');
insert into customer (first_name, phone_number)
values ('Маша', '2-222-222-22-22');


-- заполняем таблицу заказы
insert into orders (customer_id, product_id, amount, date_add)
values (1, 3, 21, '2023-01-01');
insert into orders (customer_id, product_id, amount, date_add)
values (1, 1, 101, '2023-01-14');
insert into orders (customer_id, product_id, amount, date_add)
values (2, 2, 7, '2023-01-15');
insert into orders (customer_id, product_id, amount, date_add)
values (4, 3, 31, '2023-01-15');
insert into orders (customer_id, product_id, amount, date_add)
values (3, 1, 200, '2023-01-16');
insert into orders (customer_id, product_id, amount, date_add)
values (1, 3, 201, '2023-01-30');
insert into orders (customer_id, product_id, amount, date_add)
values (1, 3, 999, now());