-- создаем таблицу товар
create table product (
    id bigserial primary key,
    name varchar(30) not null,
    price int not null
);


-- создаем таблицу покупатель
create table customer (
    id bigserial primary key,
    first_name varchar(30) not null,
    phone_number varchar(30) not null
);


-- создаем таблицу заказы
create table orders (
    id bigserial primary key,
    customer_id bigserial references customer (id),
    product_id bigserial references product (id),
    amount int default 1 check (amount > 0 and amount <= 1000),
    date_add date not null
);