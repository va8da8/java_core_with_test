-- По идентификатору заказа получить данные заказа и данные клиента,
-- создавшего этот заказ
--select name,amount,first_name,phone_number
select *
from orders
join customer c on c.id = orders.customer_id
--join product p on p.id = orders.product_id
where orders.id = 3;


-- Получить данные всех заказов одного клиента по идентификатору
-- клиента за последний месяц
select *
from orders
where customer_id = 1 and date_add > (now() - interval '1Month');


-- Найти заказ с максимальным количеством купленных цветов, вывести их
-- название и количество
select name, amount
from orders
join product p on p.id = orders.product_id
order by amount desc
limit 1;


-- Вывести общую выручку (сумму золотых монет по всем заказам) за все
-- время
select sum(price *amount) as "общая выручка"
from orders
join product p on p.id = orders.product_id;