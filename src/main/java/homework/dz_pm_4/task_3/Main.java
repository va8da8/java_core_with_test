package homework.dz_pm_4.task_3;


import java.util.List;
import java.util.function.Predicate;


/*
 * На вход подается список строк. Необходимо вывести количество непустых
 * строк в списке.
 * Например, для List.of("abc", "", "", "def", "qqq") результат равен 3.
 */
class Main {


    public static void main(String... args) {

        List<String> strings = List.of("abc", "", "", "def", "qqq");

        List<String> filteredList = strings.stream()
                .filter(Predicate.not(String::isEmpty))
                .toList();
        System.out.println(filteredList.size());
    }
}