package homework.dz_pm_4.task_6;


import java.util.*;
import java.util.stream.Collectors;


/*
 * Дан Set<Set<Integer>>. Необходимо перевести его в Set<Integer>.
 */
class Main {


    public static void main(String... args) {

        Set<Integer> set_1 = Set.of(1, 2, 3, 4);
        Set<Integer> set_2 = Set.of(5, 6, 7, 8);
        Set<Integer> set_3 = Set.of(1, 6, 4, 8);

        Set<Set<Integer>> setSet = Set.of(
                set_1,
                set_2,
                set_3
        );

        Set<Integer> flatSet = setSet.stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
        System.out.println(flatSet);
    }
}