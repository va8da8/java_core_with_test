package homework.dz_pm_4.task_2;


import java.util.List;


/*
 * На вход подается список целых чисел. Необходимо вывести результат
 * перемножения этих чисел.
 * Например, если на вход передали List.of(1, 2, 3, 4, 5), то результатом
 * должно быть число 120 (т.к. 1 * 2 * 3 * 4 * 5 = 120).
 */
class Main {


    public static void main(String... args) {

        List<Integer> integers = List.of(1, 2, 3, 4, 5);

        int multi = integers.stream()
                .mapToInt(a -> a)
                .reduce(1, (a, b) -> a * b);
        System.out.println(multi);
    }
}