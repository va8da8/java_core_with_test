package homework.dz_pm_4.task_1;


import java.util.stream.IntStream;


/*
 * Посчитать сумму четных чисел в промежутке от 1 до 100 включительно и
 * вывести ее на экран.
 */
class Main {


    public static void main(String... args) {

        int s;
        s = IntStream.rangeClosed(1, 100)
//        s = IntStream.range(1, 101)
                .filter(x -> x % 2 == 0)
                .sum();
        System.out.println(s);
    }
}