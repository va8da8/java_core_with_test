package homework.dz_pm_4.task_5;


import java.util.List;
import java.util.stream.Collectors;


/*
 * На вход подается список непустых строк. Необходимо привести все символы
 * строк к верхнему регистру и вывести их, разделяя запятой.
 * Например, для List.of("abc", "def", "qqq")
 * результат будет ABC, DEF, QQQ.
 */
class Main {


    public static void main(String... args) {

        List<String> strings = List.of("abc", "def", "qqq");

        String collect = strings.stream()
                .map(String::toUpperCase)
                .collect(Collectors.joining(", "));
        System.out.println(collect);
    }
}