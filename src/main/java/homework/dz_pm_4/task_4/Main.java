package homework.dz_pm_4.task_4;


import java.util.Comparator;
import java.util.List;


/*
 * На вход подается список вещественных чисел. Необходимо отсортировать их
 * по убыванию.
 */
class Main {


    public static void main(String... args) {

        List<Double> doubles = List.of(5.7, -1.1, -0.1, 9.567, 8.0, 0.0);

        doubles.stream()
                .sorted(Comparator.reverseOrder())
                .forEach(System.out::println);
    }
}