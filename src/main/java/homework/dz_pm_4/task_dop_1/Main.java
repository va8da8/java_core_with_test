package homework.dz_pm_4.task_dop_1;


/*
 * На вход подается две строки.
 * Необходимо определить, можно ли уравнять эти две строки, применив
 * только одну из трех возможных операций:
 *  Добавить символ.
 *  Удалить символ.
 *  Заменить символ.
 * Пример:
 * “cat” “cats” -> true
 * “cat” “cut” -> true
 * “cat” “nut” -> false
 */
class Main {


    public static void main(String... args) {

        String str_1 = "cat";
        String str_2 = "cats";
//        String str_3 = "cut";
//        String str_4 = "nut";

        System.out.println(isEqualsString(str_1, str_2));
    }


    static boolean isEqualsString(String str_1, String str_2) {

        int[] arr_1 = new int[str_2.length() + 1];
        int[] arr_2 = new int[str_2.length() + 1];

        for (int j = 0; j <= str_2.length(); j++) {

            arr_2[j] = j;
        }

        for (int i = 1; i <= str_1.length(); i++) {

            System.arraycopy
                    (arr_2, 0, arr_1, 0, arr_1.length);


            arr_2[0] = i;

            for (int j = 1; j <= str_2.length(); j++) {

                int oneOurNil =
                        (str_1.charAt(i - 1) !=
                                str_2.charAt(j - 1)) ? 1 : 0;

                arr_2[j] = Math.min(Math.min(
                                arr_1[j] + 1,
                                arr_2[j - 1] + 1),
                        arr_1[j - 1] + oneOurNil
                );
            }
        }

        int index = arr_2[arr_2.length - 1];

        return index == 1;
    }
}