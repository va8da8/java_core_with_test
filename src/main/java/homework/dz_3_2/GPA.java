package homework.dz_3_2;

import java.util.LinkedList;
import java.util.Objects;


/**
 * Класс GPA, который создает объект средняя оценка с полями - название,
 * и коллекция LinkedList<Integer> которая содержит динамический массив
 * оценок пользователя. Содержит методы get - и set- для взаимодействия с
 * полями и метод gpa() для определения средней оценки для книги.
 * Переопределены 3 метода toString() - для нужного формата вывода объекта,
 * hashCode() и equals(Object o) для отсеивания дубликатов по названию
 * книги при создании коллекции.
 */
class GPA {

    private String title;

    private LinkedList<Integer> gradPointAverage;


    public GPA(String title, LinkedList<Integer> gradPointAverage) {
        this.title = title;
        this.gradPointAverage = gradPointAverage;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LinkedList<Integer> getGradPointAverage() {

        return gradPointAverage;
    }

    public void setGradPointAverage(int i) {

        this.gradPointAverage.add(i);
    }


    public double gpa() {
        double sum = 0;

        for (Integer integer : this.gradPointAverage) {
            sum += integer;
        }
        return sum / this.gradPointAverage.size();
    }


    @Override
    public String toString() {
        return title + ", " + gradPointAverage;
    }


    @Override
    public int hashCode() {
        return Objects.hash(title);
    }


    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GPA g = (GPA) o;
        return Objects.equals(title, g.title);
    }
}
