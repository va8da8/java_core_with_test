package homework.dz_3_2;

import java.util.Objects;


/**
 * Класс Book, который создает объект книга с полями - название, автор,
 * статус "в библиотеке или у читателя", средня оценка книги. Содержит
 * методы get - и set- для взаимодействия с полями.
 * Переопределены 3 метода toString() - для нужного формата вывода объекта,
 * hashCode() и equals(Object o) для отсеивания дубликатов по названию
 * книги при создании коллекции.
 */
class Book {

    private String title;

    private String author;

    private String status;

    private double gpa;


    public Book(String title, String author, String status, double gpa) {

        this.title = title;
        this.author = author;
        this.status = status;
        this.gpa = gpa;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getGpa() {
        return gpa;
    }

    public void setGpa(double gpa) {
        this.gpa = gpa;
    }


    @Override
    public String toString() {

        return title + ", " + author + ", " + status + ", " + gpa;
    }


    @Override
    public int hashCode() {
        return Objects.hash(title);
    }


    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book b = (Book) o;
        return Objects.equals(title, b.title);
    }
}
