package homework.dz_3_2;

import java.util.Objects;


/**
 * Класс Visitor, который создает объект посетитель с полями - название
 * книги, id читателя, имя читателя, статус читателя "читает или (пустое
 * значение)". Содержит методы get - и set- для взаимодействия с
 * полями.
 * Переопределены 3 метода toString() - для нужного формата вывода объекта,
 * hashCode() и equals(Object o) для отсеивания дубликатов по id читателя
 * при создании коллекции.
 */
class Visitor {

    private String title;

    private Integer id;

    private String firstName;

    private String statusVisitor;


    public Visitor(String title, Integer id, String firstName,
                   String statusVisitor) {
        this.title = title;
        this.id = id;
        this.firstName = firstName;
        this.statusVisitor = statusVisitor;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {

        this.firstName = firstName;
    }

    public String getStatusVisitor() {
        return statusVisitor;
    }

    public void setStatusVisitor(String statusVisitor) {

        this.statusVisitor = statusVisitor;
    }


    @Override
    public String toString() {

        return id + ", " + firstName + ", " + statusVisitor;
    }


    @Override
    public int hashCode() {
        return Objects.hash(id);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Visitor v = (Visitor) o;
        return Objects.equals(id, v.id);
    }
}
