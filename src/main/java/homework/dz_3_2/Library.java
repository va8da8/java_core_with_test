package homework.dz_3_2;

import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.Set;


/**
 * Клас Library, который определяет логику работы программы "Библиотека"
 * и взаимодействие между классами Book, Visitor, GPA.
 */
class Library {

    private final static String STATUS_OUT = "у читателя";

    private final static String STATUS_IN = "в библиотеке";

    private final static String STATUS_Visitor = "читает";

    private String title;

    private String author;

    private double gpa;

    final private Scanner reader = new Scanner(System.in);


    /**
     * Метод getIntInput(), который отвечает за ввод пользователем
     * числа для выбора элемента меню.
     */
    private int getIntInput() {

        int choice = 0;
        while (choice == 0) {
            try {

                choice = reader.nextInt();
                if (choice == 0) throw new InputMismatchException();
                reader.nextLine();
            } catch (InputMismatchException e) {
                reader.nextLine();
                System.out.print("\nОШИБКА: НЕВЕРНЫЙ ВВОД. Пожалуйста," +
                        " попробуйте еще раз:");
            }
        }
        return choice;
    }


    /**
     * Метод getChoice(), который предлагает пользователю выбрать
     * нужную ему команду.
     */
    int getChoice() {

        int choice;
        System.out.println("\n\nДОБРО ПОЖАЛОВАТЬ В БИБЛИОТЕКУ ");
        System.out.println("*****************************");
        System.out.println("1) Добавить книгу");
        System.out.println("2) Удалить книгу");
        System.out.println("3) Найти книгу по названию");
        System.out.println("4) Вывести список авторов книг");
        System.out.println("5) Одолжить книгу посетителю");
        System.out.println("6) Вернуть книгу в библиотеку");
        System.out.print("\nПожалуйста, выберите " +
                "вариант (или введите -1, чтобы выйти): ");
        // получение данных от пользователя
        choice = getIntInput();
        return choice;
    }


    /**
     * Метод addBook(), который на вход принимает коллекцию книги и
     * создает новый объект книга.
     */
    public void addBook(Set<Book> b) {

        Book book;

        System.out.print
                ("\nПожалуйста, введите название книги для добавления: ");
        title = reader.nextLine();

        System.out.print
                ("\nПожалуйста, введите автора книги: ");
        author = reader.nextLine();

        // создаем новый объект Book
        book = new Book(title, author, STATUS_IN, gpa);
        // добавляем новый объект в коллекцию b
        b.add(book);

        System.out.print("\nСТАТУС: книга добавлена в библиотеку \n");
    }


    /**
     * Метод removeBook(), который на вход принимает коллекцию книги и
     * удаляет книгу, если она есть в коллекции.
     */
    public void removeBook(Set<Book> b) {

        System.out.print
                ("\nПожалуйста, введите название книги для удаления: ");
        title = reader.nextLine();
        for (Book element : b) {
            if (element.getTitle().equals(title) &&
                    element.getStatus().equals(STATUS_IN)) {

                b.remove(element);

                System.out.print("\n*** Книга удалена ***\n");
                return;
            }
            if (element.getTitle().equals(title) &&
                    element.getStatus().equals(STATUS_OUT)) {

                System.out.print("\n!!! Книга у читателя !!!\n");
                return;
            }
        }
        System.out.print("\n!!! Книга не найдена !!!\n");
    }


    /**
     * Метод searchBook(), который на вход принимает коллекцию книги и
     * выводит все данные о данной книге, если она есть в библиотеке.
     */
    public void searchBook(Set<Book> b) {

        System.out.print("\nВведите название книги, для отображения: ");
        title = reader.nextLine();

        for (Book element : b) {
            if (element.getTitle().equals(title)) {

                String[] books = element.toString().split(", ");
                System.out.println("\n\nНазвание книги: " + books[0]);
                System.out.println("Автор книги: " + books[1]);
                System.out.println("Статус книги: " + books[2]);
                System.out.println("Рейтинг книги: " + books[3]);
                return;
            }
        }
        System.out.print("\n!!! Книга не найдена !!!\n");
    }


    /**
     * Метод searchList(), который на вход принимает коллекцию книги и
     * отображает список книг у запрашиваемого автора.
     */
    public void searchList(Set<Book> b) {

        System.out.print
                ("\nВведите автора книги, для отображения списка книг: ");
        author = reader.nextLine();

        for (Book element : b) {
            String[] books = element.toString().split(", ");
            if (element.getAuthor().equals(author)) {
                System.out.print("\n*у автора: " + author +
                        ",  *книга: " + books[0]);
            }
        }
    }


    /**
     * Метод isTrueBook(), который на вход принимает коллекцию книги и
     * название книги, для проверки - находится она у читателя или нет.
     */
    public void isTrueBook(Set<Book> b, String title) {

        for (Book element : b) {
            if (element.getTitle().equals(title) &&
                    element.getStatus().equals(STATUS_OUT)) {

                System.out.print("\n!!! Книга у читателя !!!\n");
                return;
            }
        }
    }


    /**
     * Метод lendOut(), который на вход принимает коллекцию книги и
     * коллекцию посетителей. В методе происходит добавление посетителя
     * в коллекцию Set<Visitor> v, проверка на уже имеющегося
     * посетителя по id и проверка на наличие книги у посетителя.
     * При условии, что книга есть в библиотеке и она свободна, посетитель
     * может ее одолжить, если не имеет книги.
     */
    public void lendOut(Set<Book> b, Set<Visitor> v) {

        String firstName;
        Integer id;
        Visitor visitor;


        System.out.print
                ("\nПожалуйста, введите название" +
                        " книги для одалживания: ");
        title = reader.nextLine();

        isTrueBook(b, title);

        for (Book element : b) {
            if (element.getTitle().equals(title) &&
                    element.getStatus().equals(STATUS_IN)) {

                System.out.print
                        ("\nПожалуйста, введите ID читателя: ");
                id = reader.nextInt();

                reader.nextLine();
                System.out.print
                        ("\nПожалуйста, введите имя читателя: ");
                firstName = reader.nextLine();
                //создаем новый объект Visitor
                visitor = new Visitor(title, id, firstName,
                        STATUS_Visitor);
                // добавляем новый объект в коллекцию v
                v.add(visitor);

                for (Visitor el : v) {

                    // меняем статус книги
                    element.setStatus(STATUS_OUT);

                    // читатель зарегистрирован
                    if (el.getId().equals(id)) {

                        System.out.print("\n!!! читатель: " +
                                el.getFirstName() +
                                " зарегистрирован !!!\n");
                    }
                    // у читателя есть книга
                    if (el.getStatusVisitor().equals(STATUS_Visitor) &&
                            el.getId().equals(id)) {

                        System.out.print("\n!!! у читателя: " +
                                el.getFirstName() +
                                " уже есть книга: " + el.getTitle() +
                                " !!!\n");
                        return;
                    }
                    // меняем статус читателя
                    if (el.getTitle().equals("") &&
                            el.getStatusVisitor().equals("") &&
                            el.getId().equals(id)) {

                        el.setTitle(title);
                        el.setStatusVisitor(STATUS_Visitor);
                        // меняем статус книги
                        element.setStatus(STATUS_OUT);
                        System.out.println
                                ("\nСТАТУС: книга одолжена читателю\n");
                        return;
                    }
                }
            }
        }
    }


    /**
     * Метод returnBook(), который на вход принимает коллекцию книги,
     * коллекцию посетителей, коллекцию средняя оценка книги.
     * В методе происходит изменения статуса у читателя и у книги, и
     * оценка книги посетителем при ее возврате.
     */
    public void returnBook(Set<Book> b, Set<Visitor> v, Set<GPA> g) {
        GPA gpa;

        LinkedList<Integer> in = new LinkedList<>();

        int i;

        System.out.print
                ("\nПожалуйста, введите название книги для возврата: ");
        title = reader.nextLine();

        for (Book element : b) {

            if (element.getTitle().equals(title) &&
                    element.getStatus().equals(STATUS_OUT)) {

                System.out.print
                        ("\nПожалуйста оцените книгу: ");
                //reader.nextInt();
                i = reader.nextInt();

                in.add(i);
                gpa = new GPA(title, in);
                g.add(gpa);

                for (GPA e : g) {
                    if (e.getTitle().equals(title)) {

                        e.setGradPointAverage(i);
                    }
                    element.setGpa(e.gpa());
                }

                for (Visitor el : v) {
                    // меняем статус читателя
                    if (el.getTitle().equals(title) &&
                            el.getStatusVisitor()
                                    .equals(STATUS_Visitor)) {

                        element.setStatus(STATUS_IN);
                        el.setTitle("");
                        el.setStatusVisitor("");
                        System.out.print
                                ("\nСТАТУС: книга в библиотеке\n");
                    }
                }
            }
        }
    }
}
