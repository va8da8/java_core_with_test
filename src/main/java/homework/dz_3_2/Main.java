package homework.dz_3_2;


import java.util.HashSet;
import java.util.Set;


/**
 * Класс Main, который содержит метод main() для запуска программы.
 */
class Main {

    /**
     * Метод main(), который создает для классов Book, Visitor, GPA,
     * коллекции и объект класса Library для управления классами и
     * их взаимодействием.
     */
    public static void main(String... args) {

        Set<Book> b = new HashSet<>();
        Set<Visitor> v = new HashSet<>();
        Set<GPA> g = new HashSet<>();
        Library library = new Library();

        int choice = library.getChoice();

        while (choice != -1) {
            switch (choice) {
                // добавить книгу
                case 1 -> library.addBook(b);
                // удалить книгу
                case 2 -> library.removeBook(b);
                // найти книгу по названию
                case 3 -> library.searchBook(b);
                // вывести список книг по автору
                case 4 -> library.searchList(b);
                // одолжить книгу читателю
                case 5 -> library.lendOut(b, v);
                // вернуть книгу в библиотеку
                case 6 -> library.returnBook(b, v, g);

                default -> System.out.print("""

                        Вы выбрали неверный вариант.

                        """);
            }
            // ввести новый вариант.
            choice = library.getChoice();
        }
        System.out.println("\nДо новых встреч");
    }
}
