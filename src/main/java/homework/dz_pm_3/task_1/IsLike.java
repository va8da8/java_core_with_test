package homework.dz_pm_3.task_1;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/*
 * Создать аннотацию @IsLike, применимую к классу во время выполнения
 * программы. Аннотация может хранить boolean значение.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface IsLike {

    boolean isBool() default false;
}