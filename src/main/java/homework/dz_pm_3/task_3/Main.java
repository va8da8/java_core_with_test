package homework.dz_pm_3.task_3;


import java.lang.reflect.Method;


class Main {


    public static void main(String... args) {

        Class<APrinter> cls = APrinter.class;
        try {
            Object obj = cls.getDeclaredConstructor().newInstance();
            try {
                Method method =
                        cls.getDeclaredMethod("print", int.class);
                try {
                    method.invoke(obj, 4);
                } catch (Exception e) {
                    System.err.println(e.getMessage());
                }
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}