package homework.dz_pm_3.task_dop_1;


/*
 * Дана строка, состоящая из символов “(“ и “)”
 * Необходимо написать метод, принимающий эту строку и выводящий
 * результат, является ли она правильной скобочной последовательностью
 * или нет.
 * Строка является правильной скобочной последовательностью, если:
 *  Пустая строка является правильной скобочной последовательностью.
 *  Пусть S — правильная скобочная последовательность, тогда (S) есть
 * правильная скобочная последовательность.
 *  Пусть S1, S2 — правильные скобочные последовательности, тогда S1S2
 * есть правильная скобочная последовательность.
 */
class Main {


    public static void main(String... args) {

        String str = "(()()())";
//        String str = ")(";
//        String str = "(()";
//        String str = "((()))";

        System.out.println(isSequenceParentheses(str));
    }


    static boolean isSequenceParentheses(String str) {

        if (null == str || ((str.length() % 2) != 0)) {
            return false;
        } else {
            char[] ch = str.toCharArray();
            for (char c : ch) {
                if (!(c == '{' || c == '[' || c == '(' || c == '}' ||
                        c == ']' || c == ')')) {
                    return false;
                }
            }
        }

        // перебираем входную строку, удаляя парные значения
        while (str.contains("()") || str.contains("[]") ||
                str.contains("{}")) {
            str = str.replaceAll("\\(\\)", "")
                    .replaceAll("\\[\\]", "")
                    .replaceAll("\\{\\}", "");
        }
        return (str.length() == 0);
    }
}