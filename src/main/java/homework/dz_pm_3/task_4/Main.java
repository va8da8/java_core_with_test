package homework.dz_pm_3.task_4;


import java.util.*;


/*
 * Написать метод, который с помощью рефлексии получит все интерфейсы
 * класса, включая интерфейсы от классов-родителей и
 * интерфейсов - родителей.
 */
class Main {


    public static void main(String... args) {

        printInterface(ArrayList.class);
        //printInterface(LinkedHashMap.class);

        //getClassImplementsInterface(ArrayList.class);
        //getClassImplementsInterface(LinkedHashMap.class);
    }


    /**
     * Метод printInterface() получает класс и проверяет его на наличие
     * интерфейсов и выводит их(для вывода интерфейсов - родителей, мы
     * рекурсивно вызываем метод), далее проверяет классы родители и
     * каждого родителя на наличие интерфейсов.
     */
    static void printInterface(Class<?> cls) {

        for (Class<?> ignored : cls.getNestMembers()) {

            for (Class<?> cl : cls.getInterfaces()) {

                System.out.println(cl);
                printInterface(cl);
            }

            cls = cls.getSuperclass();

            if (cls == null) break;
            if (cls == Object.class) break;
        }
    }


//    /** Метод printInterfaces() выводит название класса и название
//     *  интерфейса родителя или имплементируемого интерфейса, для
//     *  вывода интерфейсов родителей мы рекурсивно вызываем этот же
//     *  метод. */
//    static void printInterfaces(Class<?> cls) {
//
//        for(Class<?> cl : cls.getInterfaces()) {
//
//            // выводим интерфейсы которые имплементирует класс
//            System.out.println(cls.getName() + " <имплементирует> " +
//                    cl.getName());
//
//            // рекурсивно проходим по интерфейсам для вывода
//            // интерфейсов родителей
//            printInterfaces(cl);
//        }
//    }
//
//
//    /** Метод getClassImplementsInterface() получает классы наследники
//     *  и сам класс с которым мы работаем, вызывает метод
//     *  printInterfaces(). */
//    static void getClassImplementsInterface(Class<?> cls) {
//
//        while (cls != Object.class) {
//
//            printInterfaces(cls);
//
//            cls = cls.getSuperclass();
//            // если у проверяемого класса нет наследников, то мы
//            // выходим из цикла
//            if (cls == null) break;
//        }
//    }
}