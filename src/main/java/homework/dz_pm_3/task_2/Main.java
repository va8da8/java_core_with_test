package homework.dz_pm_3.task_2;


import homework.dz_pm_3.task_1.IsLike;


/*
 * Написать метод, который рефлексивно проверит наличие аннотации @IsLike
 * на любом переданном классе и выведет значение, хранящееся в аннотации,
 * на экран.
 */
class Main {


    static void printAnnotationValue(Class<?> cls) {

        if (cls.isAnnotationPresent(IsLike.class)) {
            IsLike annotation = cls.getAnnotation(IsLike.class);
            System.out.println(annotation.isBool());
        }
//        else {
//            System.out.println("Класс " + cls.getSimpleName() +
//                    ": не содержит аннотации - " +
//                    IsLike.class.getSimpleName());
//        }
    }


//    @IsLike(isBool = true)
//    static class Test {}
//
//
//    @IsLike()
//    static class Test_1 {}
//
//
//    public static void main(String... args) {
//
//        System.out.println();
//
//        printAnnotationValue(Test.class);
//        printAnnotationValue(Test_1.class);
//        printAnnotationValue(Main.class);
//    }
}