package homework.dz_pm_3.task_dop_2;


import java.util.Deque;
import java.util.LinkedList;


/*
 * Дана строка, состоящая из символов “(“, “)”, “{”, “}”, “[“, “]”
 * Необходимо написать метод, принимающий эту строку и выводящий
 * результат, является ли она правильной скобочной последовательностью
 * или нет.
 * Условия для правильной скобочной последовательности те, же что и в
 * задаче 1, но дополнительно:
 *  Открывающие скобки должны быть закрыты однотипными закрывающими
 * скобками.
 *  Каждой закрывающей скобке соответствует открывающая скобка того же
 * типа.
 */
class Main {


    public static void main(String... args) {

        String str = "{()[]()}";
//        String str = "{)(}";
//        String str = "[}";
//        String str = "[{(){}}][()]{}";

        System.out.println(isSequenceBracket(str));
    }


    static boolean isSequenceBracket(String str) {

        Deque<Character> stack = new LinkedList<>();
        for (char ch : str.toCharArray()) {
            if (ch == '{' || ch == '[' || ch == '(') {
                stack.addFirst(ch);
            } else {
                if (!stack.isEmpty()
                        && ((stack.peekFirst() == '{' && ch == '}')
                        || (stack.peekFirst() == '[' && ch == ']')
                        || (stack.peekFirst() == '(' && ch == ')'))) {
                    stack.removeFirst();
                } else {
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }
}