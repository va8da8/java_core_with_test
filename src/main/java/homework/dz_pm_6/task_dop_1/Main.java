package homework.dz_pm_6.task_dop_1;


/*
 * Напишите программу для проверки, является ли введение число - числом
 * Армстронга
 * Число Армстронга — натуральное число, которое в данной системе
 * счисления равно сумме своих цифр, возведенных в степень, равную
 * количеству его цифр. Иногда, чтобы считать число таковым, достаточно,
 * чтобы степени, в которые возводятся цифры, были равны m. Например,
 * десятичное число
 * 153— число Армстронга, потому что 1^3 + 5^3 + 3^3 = 153.
 */
class Main {


    public static void main(String... args) {

        System.out.println(isArmstrongNumber(10));
        System.out.println(isArmstrongNumber(4_679_307_774L));
    }


    static boolean isArmstrongNumber(long num) {

        long result = 0;
        long temp = num;
        long countNumber = (long) Math.log10(num) + 1;

        while (temp != 0) {

            result += Math.pow(temp % 10, countNumber);
            temp /= 10;
        }
        return num == result;
    }
}