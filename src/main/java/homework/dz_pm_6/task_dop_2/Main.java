package homework.dz_pm_6.task_dop_2;


/*
 * Напишите программы, чтобы узнать, является ли введенное число простым
 * или нет.
 */
class Main {


    public static void main(String... args) {

        System.out.println(isSimpleNumber(4));
        System.out.println(isSimpleNumber(5));
    }


    static boolean isSimpleNumber(int number) {

        if (number < 2) return false;

        for (int i = 2; i <= number / 2; i++) {

            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }
}