package homework.dz_1_1.task_9;


import java.util.Scanner;


/*
 * На вход подается бюджет мероприятия – n тугриков. Бюджет на одного
 * гостя – k тугриков. Вычислите и выведите, сколько гостей можно
 * пригласить на мероприятие.
 * Ограничения: 0 < n < 100_000 0 < k < 1000 k<n
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int k = scanner.nextInt();

        System.out.println(new Main().количествоГостей(n, k));
    }


    public int количествоГостей(int n, int k) {

        if ((n < 1 || n > 99_999) || (k < 1 || k > 999) || (k > n)) {

            throw new IllegalArgumentException
                    ("Введено недопустимое значение");
        }
        return n / k;
    }
}