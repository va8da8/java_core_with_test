package homework.dz_1_1.task_5;


import java.util.Scanner;


/*
 * Переведите дюймы в сантиметры (1 дюйм = 2,54 сантиметров). На вход
 * подается количество дюймов, выведите количество сантиметров.
 * Ограничения: 0 < inches < 1000
 */
class Main {

    static final double inch = 2.54;


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int inches = scanner.nextInt();

        System.out.println(new Main().inchesToCentimeters(inches, inch));
    }


    public double inchesToCentimeters(int inches, double inch) {

        if (inches < 1 || inches > 999) {

            throw new IllegalArgumentException
                    ("Введено недопустимое значение");
        }
        return inch * inches;
    }
}