package homework.dz_1_1.task_8;


import java.util.Scanner;


/*
 * На вход подается баланс счета в банке – n. Рассчитайте дневной бюджет
 * на 30 дней.
 * Ограничения: 0 < n < 100_000
 */
class Main {

    static final double days = 30;


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        System.out.println(new Main().dailyBudget(n, days));
    }


    public double dailyBudget(int n, double days) {

        if (n < 1 || n > 99_999) {

            throw new IllegalArgumentException
                    ("Введено недопустимое значение");
        }
        return n / days;
    }
}