package homework.dz_1_1.task_7;


import java.util.Scanner;


/*
 * На вход подается двузначное число n. Выведите число, полученное
 * перестановкой цифр в исходном числе n. Если после перестановки
 * получается ведущий 0, его также надо вывести.
 * Ограничения: 9 < n < 100
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        System.out.println(new Main().permutationNumbers(n));
    }


    public String permutationNumbers(int n) {

        int firstNumber = n / 10;
        int secondNumber = n % 10;

        if (n < 10 || n > 99) {

            throw new IllegalArgumentException
                    ("Введено недопустимое значение");
        }
        return secondNumber + "" + firstNumber;
    }
}