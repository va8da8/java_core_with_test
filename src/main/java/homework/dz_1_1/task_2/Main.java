package homework.dz_1_1.task_2;


import java.util.Scanner;


/*
 * На вход подается два целых числа a и b. Вычислите и выведите среднее
 * квадратическое a и b.
 * Подсказка:
 *  Для вычисления квадратного корня воспользуйтесь функцией Math.sqrt(x)
 * Ограничения: 0 < a, b < 100
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int a = scanner.nextInt();
        int b = scanner.nextInt();

        System.out.println(new Main().rootMeanSquare(a, b));
    }


    public double rootMeanSquare(int a, int b) {

        if ((a < 1 || a > 99) || (b < 1 || b > 99)) {

            throw new IllegalArgumentException
                    ("Введено недопустимое значение");
        }
        return Math.sqrt((Math.pow(a, 2) + Math.pow(b, 2)) / 2);
    }
}