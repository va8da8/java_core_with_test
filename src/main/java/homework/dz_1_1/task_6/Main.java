package homework.dz_1_1.task_6;


import java.util.Scanner;


/*
 * На вход подается количество километров count. Переведите километры
 *  в мили (1 миля = 1,60934 км) и выведите количество миль.
 * Ограничения: 0 < count < 1000
 */
class Main {

    static final double mile = 1.60934;


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int count = scanner.nextInt();

        System.out.println(new Main().kilometersToMiles(count, mile));
    }


    public double kilometersToMiles(int count, double mile) {

        if (count < 1 || count > 999) {

            throw new IllegalArgumentException
                    ("Введено недопустимое значение");
        }
        return count / mile;
    }
}