package homework.dz_1_1.task_4;


import java.util.Scanner;


/*
 * На вход подается количество секунд, прошедших с начала текущего
 * дня – count. Выведите в консоль текущее время в формате: часы и минуты.
 * Ограничения: 0 < count < 86400
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int count = scanner.nextInt();

        System.out.println(new Main().time(count));
    }


    public String time(int count) {

        if (count < 1 || count > 86399) {

            throw new IllegalArgumentException
                    ("Введено недопустимое значение");
        }
        int hours = count / 3600;
        int minutes = count / 60 % 60;
        //int seconds = count / 1 % 60;
        return hours + " " + minutes;
    }
}