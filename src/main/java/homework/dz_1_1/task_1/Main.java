package homework.dz_1_1.task_1;


import java.util.Scanner;


/*
 * Вычислите и выведите на экран объем шара, получив его радиус r с
 * консоли.
 * Подсказка: считать по формуле V = 4/3 * pi * r^3. Значение числа pi
 * взять из Math.
 * Ограничения: 0 < r < 100
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int radius = scanner.nextInt();

        System.out.println(new Main().ballVolume(radius));
    }


    public double ballVolume(int radius) {

        if (radius < 1 || radius > 99) {

            throw new IllegalArgumentException
                    ("Введено недопустимое значение");
        }
        return 4 * Math.PI * Math.pow(radius, 3) / 3;
    }
}