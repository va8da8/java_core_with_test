package homework.dz_1_3.task_7;


import java.util.Scanner;


/*
 * Дана строка s. Вычислить количество символов в ней, не считая
 * пробелов (необходимо использовать цикл).
 * Ограничения: 0 < s.length() < 1000
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        String str = scanner.nextLine();

        System.out.println(new Main().stringLength(str));
    }


    public int stringLength(String str) {

        if (str.length() > 1_000) {

            throw new IllegalArgumentException
                    ("Введено недопустимое значение");
        }
        int count = 0;
        for (int i = 0; i < str.toCharArray().length; i++) {

            if (str.toCharArray()[i] == ' ') continue;
            else count++;
        }
        return count;
    }
}