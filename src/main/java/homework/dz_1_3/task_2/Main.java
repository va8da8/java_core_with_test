package homework.dz_1_3.task_2;


import java.util.Scanner;


/*
 * На вход подается два положительных числа m и n. Найти сумму чисел
 * между m и n включительно.
 * Ограничения: 0 < m, n < 10 m<n
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int m = scanner.nextInt();
        int n = scanner.nextInt();

        System.out.println(new Main().sumBetweenNumbers(m, n));
    }


    public int sumBetweenNumbers(int m, int n) {


        if (((m < 1 || m > 9) || (n < 1 || n > 9))
                || (m > n)) {

            throw new IllegalArgumentException
                    ("Введено недопустимое значение");
        }
        int sum = 0;
        for (int i = m; i <= n; i++) {

            sum += i;
        }
        return sum;
    }
}