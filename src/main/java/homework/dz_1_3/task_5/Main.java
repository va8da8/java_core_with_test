package homework.dz_1_3.task_5;


import java.util.Scanner;


/*
 * Даны положительные натуральные числа m и n. Найти остаток от деления
 * m на n, не выполняя операцию взятия остатка.
 * Ограничения: 0 < m, n < 10
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int m = scanner.nextInt();
        int n = scanner.nextInt();

        System.out.println(new Main().remainderOfDivision(m, n));
    }


    public int remainderOfDivision(int m, int n) {

        if ((n < 1 || n > 9) || (m < 1 || m > 9)) {

            throw new IllegalArgumentException
                    ("Введено недопустимое значение");
        }
        return (m - n * (m / n));
    }
}