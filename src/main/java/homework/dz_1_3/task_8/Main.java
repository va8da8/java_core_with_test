package homework.dz_1_3.task_8;


import java.util.Scanner;


/*
 * На вход подается:
 *  целое число n,
 *  целое число p
 *  целые числа a1, a2 , ... an
 * Необходимо вычислить сумму всех чисел a1, a2, a3 ... an которые
 * строго больше p.
 * Ограничения: 0 < p, n, ai < 1000
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int p = scanner.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {

            arr[i] = scanner.nextInt();
        }
        System.out.println(new Main().sumOfNumbersGreaterThanP(n, p, arr));
    }


    public int sumOfNumbersGreaterThanP(int n, int p, int[] arr) {

        if (n <= 0 || n > 1000) {

            throw new IllegalArgumentException
                    ("n должно быть в диапазоне от 1 до 1000");
        }
        if (p <= 0 || p >= 1000) {

            throw new IllegalArgumentException
                    ("p должно быть в диапазоне от 1 до 999");
        }
        int sum = 0;
        for (int i = 0; i < n; i++) {
            if (arr[i] <= 0 || arr[i] >= 1000) {

                throw new IllegalArgumentException
                        ("ai должно быть в диапазоне от 1 до 999");
            }
            if (arr[i] > p) {

                sum += arr[i];
            }
        }
        return sum;
    }
}