package homework.dz_1_3.task_9;


import java.util.Scanner;


/*
 * На вход последовательно подается возрастающая последовательность из
 * n целых чисел, которая может начинаться с отрицательного числа.
 * Посчитать и вывести на экран, какое количество отрицательных чисел
 * было введено в начале последовательности. Помимо этого нужно
 * прекратить выполнение цикла при получении первого неотрицательного
 * числа на вход.
 * Ограничения: 0 < n < 1000 -1000 < ai < 1000
 */
class Main {


    public static void main(String... args) {

        System.out.println(new Main().sumNegativeNumbers());
    }


    public int sumNegativeNumbers() {

        Scanner scanner = new Scanner(System.in);

        int n = 999;
        int countNegatives = 0;

        for (int i = 0; i < n; i++) {

            int number = scanner.nextInt();
            if (number < -999 || number > 999) {

                throw new IllegalArgumentException
                        ("Введено недопустимое значение");
            }
            if (number < 0) {

                countNegatives++;
            } else {
                break;
            }
        }
        return countNegatives;
    }
}