package homework.dz_1_3.task_6;


import java.util.Scanner;


/*
 * В банкомате остались только купюры номиналом 8 4 2 1. Дано
 * положительное число n - количество денег для размена. Необходимо
 * найти минимальное количество купюр с помощью которых можно
 * разменять это количество денег (соблюсти порядок: первым числом
 * вывести количество купюр номиналом 8, вторым - 4 и т д)
 * Ограничения: 0 < n < 1000000
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        minNumberOfBanknotes(n);
    }


    public static void minNumberOfBanknotes(int n) {

        if (n < 1 || n > 999_999) {

            throw new IllegalArgumentException
                    ("Введено недопустимое значение");
        }
        int banknote = 8;
        int countOfBanknotes;
        for (int i = banknote; i >= 1; ) {
            countOfBanknotes = n / i;
            n = n % i;

            System.out.print(countOfBanknotes + " ");

            i = i / 2;
        }
    }
}