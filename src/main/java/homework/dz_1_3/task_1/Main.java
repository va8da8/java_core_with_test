package homework.dz_1_3.task_1;


/*
 * Напечатать таблицу умножения от 1 до 9. Входных данных нет.
 */
class Main {


    public static void main(String... args) {

        printMultiplicationTable();
    }


    public static void printMultiplicationTable() {

        for (int i = 1; i <= 9; i++) {
            for (int j = 1; j <= 9; j++) {

                System.out.print(i + "x" + j + "=" + i * j + " ");
            }
            System.out.println();
        }
    }
}