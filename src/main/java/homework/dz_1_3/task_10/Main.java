package homework.dz_1_3.task_10;


import java.util.Scanner;


/*
 * Вывести на экран “ёлочку” из символа звездочки (*) заданной высоты N.
 * На N + 1 строке у “ёлочки” должен быть отображен ствол из символа |
 * Ограничения: 2 < n < 10
 */
class Main {


    public static void main(String... args) {

        new Main().printTree();
    }


    public void printTree() {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        if (n <= 2 || n >= 10) {

            throw new IllegalArgumentException
                    ("Введено недопустимое значение");
        }
        // выводим верхнюю часть ёлки
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n - i; j++) {

                System.out.print(" ");
            }
            for (int j = 1; j <= 2 * i - 1; j++) {

                System.out.print("*");
            }
            System.out.println();
        }
        // выводим ствол ёлки
        for (int i = 1; i <= n - 1; i++) {

            System.out.print(" ");
        }
        System.out.println("|");
    }
}