package homework.dz_1_3.task_4;


import java.util.Scanner;


/*
 * Дано натуральное число n. Вывести его цифры в “столбик”.
 * Ограничения: 0 < n < 1000000
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        numbersInAColumn(n);
    }


    public static void numbersInAColumn(int n) {

        if ((n < 1 || n > 999_999)) {

            throw new IllegalArgumentException
                    ("Введено недопустимое значение");
        }
        if (n < 10) System.out.println(n);
        else {
            numbersInAColumn(n / 10);
            System.out.println(n % 10);
        }
    }
}