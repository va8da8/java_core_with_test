package homework.dz_1_3.task_3;


import java.util.Scanner;


/*
 * На вход подается два положительных числа m и n. Необходимо
 * вычислить m^1 + m^2 + ... + m^n
 * Ограничения: 0 < m, n < 10
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int m = scanner.nextInt();
        int n = scanner.nextInt();

        System.out.println(new Main().rootOfNumbers(m, n));
    }


    public int rootOfNumbers(int m, int n) {

        if ((m < 1 || m > 9) || (n < 1 || n > 9)) {

            throw new IllegalArgumentException
                    ("Введено недопустимое значение");
        }
        int sum = 0;
        for (int i = 1; i <= n; i++) {

            sum += Math.pow(m, i);
        }
        return sum;
    }
}