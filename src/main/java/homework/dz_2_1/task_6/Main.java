package homework.dz_2_1.task_6;


import java.util.Scanner;


/*
 * На вход подается строка S, состоящая только из русских заглавных
 * букв (без Ё).
 * Необходимо реализовать метод, который кодирует переданную строку
 * с помощью азбуки Морзе и затем вывести результат на экран. Отделять
 * коды букв нужно пробелом.
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        String str = scanner.nextLine();

        String[] morse = {".-", "-...", ".--", "--.", "-..", ".",
                "...-", "--..", "..", ".---", "-.-", ".-..", "--",
                "-.", "---", ".--.", ".-.", "...", "-", "..-", "..-.",
                "....", "-.-.", "---.", "----", "--.-", "--.--",
                "-.--", "-..-", "..-..", "..--", ".-.-"};

        char[] alphabet = new char[morse.length];

        for (int i = 'А', j = 0; i <= 'Я'; i++) {

            alphabet[j] = (char) i;
            j++;
        }

        code(morse, alphabet, str);
    }


    public static void code(String[] morse,
                            char[] alphabet, String str) {

        StringBuilder strMorse = new StringBuilder();
        char[] ch = str.toCharArray();

        for (char c : ch) {
            for (int j = 0; j < alphabet.length; j++) {
                if (alphabet[j] == c) {
                    strMorse.append(morse[j]).append(" ");
                    break;
                }
            }
        }
        System.out.println(strMorse);
    }
}