package homework.dz_2_1.task_3;


import java.util.Scanner;


/*
 * На вход подается число N — длина массива. Затем передается массив
 * целых чисел (ai) из N элементов, отсортированный по возрастанию.
 * После этого вводится число X — элемент, который нужно добавить в
 * массив, чтобы сортировка в массиве сохранилась.
 * Необходимо вывести на экран индекс элемента массива, куда нужно
 * добавить X. Если в массиве уже есть число равное X, то X нужно
 * поставить после уже существующего.
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[] arr = new int[n];

        for (int i = 0; i < arr.length; i++) {

            arr[i] = scanner.nextInt();
        }

        int x = scanner.nextInt();

        System.out.println(indexElementOfArray(arr, x));
    }


    public static int indexElementOfArray(int[] arr, int x) {

        int index = 0;

        for (int i = 0; i < arr.length; i++) {

            if (arr[i] > x) {
                index = i;
                break;
            }
        }
        return index;
    }
}