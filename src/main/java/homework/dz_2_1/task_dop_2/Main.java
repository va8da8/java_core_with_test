package homework.dz_2_1.task_dop_2;


import java.util.Scanner;


/*
 * На вход подается число N — длина массива. Затем передается массив целых
 * чисел (ai) из N элементов, отсортированный по возрастанию.
 * Необходимо создать массив, полученный из исходного - возведением в
 * квадрат каждого элемента, упорядочить элементы по возрастанию и вывести
 *  их на экран.
 * Ограничения:  0<N<100  -1000 < ai < 1000
 * Решить задачу за линейное время.
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        // длинна массива
        int n = scanner.nextInt();

        int[] num = new int[n];

        for (int i = 0; i < n; i++) {
            num[i] = scanner.nextInt();
        }

        // возводим элементы массива в степень
        for (int i = 0; i < num.length; i++) {
            num[i] = (int) Math.pow(num[i], 2);
        }

        // сортируем массив проходя по нему n раз
        int q = 0;
        while (q < n) {
            for (int i = 0; i < num.length - 1; i++) {
                int t;
                if (num[i] > num[i + 1]) {
                    t = num[i];
                    num[i] = num[i + 1];
                    num[i + 1] = t;
                }
            }
            q++;
        }

        // выводим массив
        for (int j : num) {
            System.out.print(j + " ");
        }
    }
}