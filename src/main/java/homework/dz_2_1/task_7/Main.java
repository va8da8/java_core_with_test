package homework.dz_2_1.task_7;


import java.util.Arrays;
import java.util.Scanner;


/*
 * На вход подается число N — длина массива. Затем передается массив
 * целых чисел (ai) из N элементов, отсортированный по возрастанию.
 * Необходимо создать массив, полученный из исходного-возведением
 * в квадрат каждого элемента, упорядочить элементы по возрастанию
 * и вывести их на экран.
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[] arr = new int[n];

        for (int i = 0; i < arr.length; i++) {

            arr[i] = scanner.nextInt();
        }

        elementsInASquare(arr);
    }


    public static void elementsInASquare(int[] arr) {

        for (int i = 0; i < arr.length; i++) {

            arr[i] = (int) Math.pow(arr[i], 2);
        }

        Arrays.sort(arr);

        for (int element : arr)
            System.out.print(element + " ");
    }
}