package homework.dz_2_1.task_4;


import java.util.*;


/*
 * На вход подается число N — длина массива. Затем передается массив
 * целых чисел (ai) из N элементов, отсортированный по возрастанию.
 * Необходимо вывести на экран построчно, сколько встретилось различных
 * элементов. Каждая строка должна содержать количество элементов и сам
 * элемент через пробел.
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[] arr = new int[n];

        for (int i = 0; i < arr.length; i++) {

            arr[i] = scanner.nextInt();
        }

        elementAndQuantity(arr);
    }


    public static void elementAndQuantity(int[] arr) {

        for (int i = 0; i < arr.length; i++) {
            int count = 1;
            for (int j = i + 1; j < arr.length; j++) {

                if (arr[i] != arr[j]) {
                    break;
                }
                count++;
            }
            System.out.print(count + " " + arr[i]);
            System.out.println();
            i += count - 1;
        }

//        Map<Integer, Integer> integerMap = new HashMap<>();
//        List<Integer> integerList = new ArrayList<>();
//
//        for (int el : arr) {
//            integerList.add(el);
//        }
//
//        for (Integer temp : integerList) {
//
//            if (!integerMap.containsKey(temp)) {
//                integerMap.put(temp, 1);
//            } else {
//                integerMap.put(temp, integerMap.get(temp) + 1);
//            }
//        }
//
//        for (Map.Entry<Integer, Integer>
//                entry : integerMap.entrySet()) {
//
//            System.out.println(
//                    entry.getValue() + " " + entry.getKey());
//        }

//        int count = 1;
//
//        for (int i = 0; i < arr.length - 1; i++) {
//
//            if (arr[i] == arr[i + 1]) {
//
//                count++;
//
//                if (i == arr.length - 2) {
//
//                    System.out.println(count + " " + arr[i]);
//                }
//            }
//            if (arr[i] != arr[i + 1]) {
//
//                System.out.println(count + " " + arr[i]);
//
//                count = 1;
//
//                if (i == arr.length - 2) {
//
//                    System.out.println(count + " " + arr[i + 1]);
//                }
//            }
//        }
    }
}