package homework.dz_2_1.task_1;


import java.util.Scanner;


/*
 * На вход подается число N — длина массива. Затем передается массив
 * вещественных чисел (ai) из N элементов.
 * Необходимо реализовать метод, который принимает на вход полученный
 * массив и возвращает среднее арифметическое всех чисел массива.
 * Вывести среднее арифметическое на экран.
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        double[] arr = new double[n];

        for (int i = 0; i < arr.length; i++) {

            arr[i] = scanner.nextDouble();
        }

        System.out.println(averageOfArray(arr));
    }


    public static double averageOfArray(double[] arr) {

        double sum = 0;

        for (double element : arr) {

            sum += element;
        }

        return sum / arr.length;
    }
}