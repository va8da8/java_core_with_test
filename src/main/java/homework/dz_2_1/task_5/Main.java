package homework.dz_2_1.task_5;


import java.util.Scanner;


/*
 * На вход подается число N — длина массива. Затем передается массив
 * целых чисел (ai) из N элементов. После этого передается число
 * M — величина сдвига.
 * Необходимо циклически сдвинуть элементы массива на M элементов вправо.
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[] arr = new int[n];

        for (int i = 0; i < arr.length; i++) {

            arr[i] = scanner.nextInt();
        }

        int m = scanner.nextInt();

        arrayShift(arr, m);
    }


    public static void arrayShift(int[] arr, int m) {

        for (int i = 0; i < m; i++) {
            int temp = arr[arr.length - 1];
            for (int j = arr.length - 1; j > 0; j--) {

                arr[j] = arr[j - 1];
            }
            arr[0] = temp;
        }

        for (int element : arr)
            System.out.print(element + " ");
    }
}