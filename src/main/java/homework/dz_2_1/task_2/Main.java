package homework.dz_2_1.task_2;


import java.util.Scanner;


/*
 * На вход подается число N — длина массива. Затем передается массив
 * целых чисел (ai) из N элементов. После этого аналогично передается
 * второй массив (aj) длины M.
 * Необходимо вывести на экран true, если два массива одинаковы
 * (то есть содержат одинаковое количество элементов и для каждого i == j
 * элемент ai == aj). Иначе вывести false.
 */
class Main {

    static Scanner scanner = new Scanner(System.in);


    public static void main(String... args) {

        int n = scanner.nextInt();
        double[] arr_n = new double[n];

        for (int i = 0; i < arr_n.length; i++) {

            arr_n[i] = scanner.nextDouble();
        }

        int m = scanner.nextInt();
        double[] arr_m = new double[m];

        for (int i = 0; i < arr_m.length; i++) {

            arr_m[i] = scanner.nextDouble();
        }

        System.out.println(isEqualsArrays(arr_n, arr_m));
    }


    public static boolean isEqualsArrays(double[] arr_n, double[] arr_m) {

        if (arr_n.length != arr_m.length) return false;

        for (int i = 0; i < arr_n.length; i++) {
            for (int j = 0; j < arr_m.length; j++) {

                if (arr_n[i] != arr_m[i]) return false;
            }
        }
        return true;
    }
}