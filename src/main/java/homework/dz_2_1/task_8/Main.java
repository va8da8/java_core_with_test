package homework.dz_2_1.task_8;


import java.util.Arrays;
import java.util.Scanner;


/*
 * На вход подается число N — длина массива. Затем передается массив
 * целых чисел (ai) из N элементов. После этого передается число M.
 * Необходимо найти в массиве число, максимально близкое к M
 * (т.е. такое число, для которого |ai - M| минимальное). Если их
 * несколько, то вывести максимальное число.
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[] arr = new int[n];

        for (int i = 0; i < arr.length; i++) {

            arr[i] = scanner.nextInt();
        }

        int m = scanner.nextInt();

        System.out.println(findElement(m, arr));
    }


    public static int findElement(int m, int... arr) {

        int element = 0;
        Arrays.sort(arr);
        long value = 2L * Integer.MAX_VALUE;

        // Проходим по массиву и сравниваем разницу, сравнение
        // по модулю, т.е. чем меньше разница - тем ближе числа
        for (int el : arr) {

            if (Math.abs(el - m) > value) {
                continue;
            }

            value = Math.abs(el - m);
            element = el;
        }
        return element;
    }
}