package homework.dz_2_1;


import java.util.Scanner;

/*
* Создать программу генерирующую пароль.
* На вход подается число N — длина желаемого пароля. Необходимо проверить,
* что N >= 8, иначе вывести на экран "Пароль с N количеством символов
* небезопасен" (подставить вместо N число) и предложить пользователю еще раз
* ввести число N.
* Если N >= 8 то сгенерировать пароль, удовлетворяющий условиям ниже и
* вывести его на экран. В пароле должны быть:
*  заглавные латинские символы
*  строчные латинские символы
*  числа
*  специальные знаки(_*-)
*/
public class Task20_1 {

    public static final String UPPER_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static final String LOWER_CHARS = "abcdefghijklmnopqrstuvwxyz";
    public static final String NUMBERS = "1234567890";
    public static final String SPECIAL_CHARS = "_*-";


    /** Метод main который запускает программу */
    public static void main(String... args) {

        gen();
    }


    /** Метод gen который принимает число(длина пароля), если длина больше
     * 7, то вызывается метод pas, иначе повторно ввести число */
    static void gen(){

        Scanner scanner = new Scanner(System.in);

        int n;

        do {
            // длина желаемого пароля
            n = scanner.nextInt();

            if (n < 8) {
                System.out.println("Пароль с " + n +
                        " количеством символов небезопасен");
                System.out.println("Введите число символов больше 7 ");
            } else {
                pas(n);
            }
        } while (n < 8);
    }


    /** Метод pas который генерирует случайный пароль и проверяет его
     * на вхождение всех значений подтверждающих его надежность. */
    static String pas(int n){

        String pat =
                "(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\\*\\-\\_]).{8,}";

        String password = "";

        for (int i = 0; i < n; i++) {
            int rand = (int) (4 * Math.random());

            switch (rand) {
                case 0:
                    rand = (int)
                            (SPECIAL_CHARS.length() * Math.random());
                    password += String.valueOf
                            (SPECIAL_CHARS.charAt(rand));
                    break;
                case 1:
                    rand = (int)
                            (NUMBERS.length() * Math.random());
                    password += String.valueOf
                            (NUMBERS.charAt(rand));
                    break;
                case 2:
                    rand = (int)
                            (LOWER_CHARS.length() * Math.random());
                    password += String.valueOf
                            (LOWER_CHARS.charAt(rand));
                    break;
                case 3:
                    rand = (int)
                            (UPPER_CHARS.length() * Math.random());
                    password += String.valueOf
                            (UPPER_CHARS.charAt(rand));
                    break;
            }
        }

        if (password.matches(pat)) {
            System.out.println(password);
        } else {
            pas(n);
        }
        return password;
    }
}
