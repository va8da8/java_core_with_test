package homework.dz_2_1.task_9;


import java.util.*;


/*
 * На вход подается число N — длина массива. Затем передается массив
 * строк из N элементов (разделение через перевод строки). Каждая
 * строка содержит только строчные символы латинского алфавита.
 * Необходимо найти и вывести дубликат на экран. Гарантируется что
 * он есть и только один.
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        scanner.nextLine();

        String[] mass = new String[n];

        for (int i = 0; i < mass.length; i++) {

            mass[i] = scanner.nextLine();
        }

        System.out.println(duplicateElement(mass));
    }


    public static String duplicateElement(String[] mass) {

        String element = "";

        for (int i = 0; i < mass.length; i++) {
            for (int j = i + 1; j < mass.length; j++) {

                if (mass[i].equals(mass[j])) {
                    element = mass[i];
                    break;
                }
            }
        }
        return element;
    }


//    public static <T> List<T> duplicatesElements(T...mass) {
//
//        Set<T> duplicates = new HashSet<>();
//
//        for (int i = 0; i < mass.length; i++) {
//
//            T e1 = mass[i];
//            // игнорируем null
//            if (e1 == null) continue;
//            // сравниваем каждый элемент со всеми остальными
//            for (int j = 0; j < mass.length; j++) {
//
//                // не проверяем элемент с собой же
//                if (i == j) continue;
//                T e2 = mass[j];
//                if (e1.equals(e2)) {
//                    // дубликат найден, сохраним его
//                    duplicates.add(e2);
//                }
//            }
//        }
//        return new ArrayList<>(duplicates);
//    }
}