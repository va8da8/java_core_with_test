package homework.dz_3_1.task_7;


/**
 * Класс Main который содержит единственный метод main, вызывает
 * статический
 * метод triangleLength() из класса TriangleChecker, без создания объекта.
 */
class Main {


    public static void main(String... args) {

        TriangleChecker.triangleLength(5.9, 6.1, 7.1);
        TriangleChecker.triangleLength(55.3, 6.6, 7.4);
    }
}
