package homework.dz_3_1.task_3;

import homework.dz_3_1.task_2.Student;

import java.util.Arrays;
import java.util.Comparator;


/*
 * Необходимо реализовать класс StudentService.
 * У класса должны быть реализованы следующие публичные методы:
 *   bestStudent() — принимает массив студентов (класс Student из
 * предыдущего задания), возвращает лучшего студента (т.е. который имеет
 * самый высокий средний балл). Если таких несколько — вывести любого.
 *   sortBySurname() — принимает массив студентов (класс Student из
 * предыдущего задания) и сортирует его по фамилии.
 */
public class StudentService {


    /**
     * Метод принимает массив студентов, выводит лучшего студента.
     */
    public void bestStudent(Student[] students) {

        Arrays.sort(students, new Comparator<Student>() {
            @Override
            public int compare(Student students1, Student students2) {

                if (students1.gpa() == students2.gpa()) {
                    return 0;
                } else if (students1.gpa() > students2.gpa()) {
                    return -1;
                } else {
                    return 1;
                }
            }
        });

        for (int i = 0; i < students.length; i++) {

            System.out.println(students[0]);
            break;
        }
    }


    /**
     * Метод принимает массив студентов и сортирует его по фамилии.
     */
    public void sortBySurname(Student[] students) {

        Arrays.sort(students);
    }
}
