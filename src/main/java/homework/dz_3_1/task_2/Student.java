package homework.dz_3_1.task_2;

import java.util.Arrays;


/*
 * Необходимо реализовать класс Student.
 * У класса должны быть следующие приватные поля:
 *   String name — имя студента
 *   String surname — фамилия студента
 *   int[] grades — последние 10 оценок студента. Их может быть меньше,
 * но не может быть больше 10.
 * И следующие публичные методы:
 *   геттер/сеттер для name
 *   геттер/сеттер для surname
 *   геттер/сеттер для grades
 *   метод, добавляющий новую оценку в grades. Самая первая оценка
 * должна быть удалена, новая должна сохраниться в конце массива (т.е.
 * массив должен сдвинуться на 1 влево)
 *   метод, возвращающий средний балл студента (рассчитывается как
 * среднее арифметическое от всех оценок в массиве grades)
 */
public class Student implements Comparable<Student> {

    private String name;

    private String surname;

    private int[] grades;


    public Student(String name, String surname, int[] grades) {
        this.name = name;
        this.surname = surname;
        this.grades = grades;
    }


    /**
     * Методы set- и get-
     */
    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getSurname() {
        return surname;
    }


    public void setSurname(String surname) {
        this.surname = surname;
    }


    public int[] getGrades() {
        return grades;
    }


    public void setGrades(int[] grades) {
        this.grades = grades;
    }


    /**
     * Метод newAverage() для добавления новой оценки в конец
     * массива и удаление перво.
     */
    // добавление новой оценки
    public void newAverage(int newAverage) {

        int temp = newAverage;

        for (int i = 0; i < this.grades.length - 1; i++) {
            this.grades[i] = this.grades[i + 1];
        }

        this.grades[this.grades.length - 1] = temp;
    }


    /**
     * Метод gpa() для подсчета и вывода среднего балла.
     */
    public int gpa() {

        int gradPointAverage;
        int sum = 0;

        for (int i = 0; i < this.grades.length; i++) {
            sum += this.grades[i];
        }

        gradPointAverage = sum / this.grades.length;

        return gradPointAverage;
    }


    /**
     * Переопределение метода toString(), для корректного вывода
     * данных в классе Student.
     */
    @Override
    public String toString() {

        return name + ", " + surname + ", " + Arrays.toString(grades);
    }


    /**
     * Переопределение метода compareTo(), для сортировки в классе
     * Student по фамилии.
     */
    @Override
    public int compareTo(Student student) {

        return this.surname.compareTo(student.getSurname());
    }
}