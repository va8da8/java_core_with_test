package homework.dz_3_1.task_6;


import java.util.Arrays;

/*
 * Необходимо реализовать класс AmazingString, который хранит внутри себя
 * строку как массив char и предоставляет следующий функционал:
 * Конструкторы:
 *   Создание AmazingString, принимая на вход массив char
 *   Создание AmazingString, принимая на вход String
 * Публичные методы (названия методов, входные и выходные параметры
 * продумать самостоятельно). Все методы ниже нужно реализовать “руками”,
 *  т.е. не прибегая к переводу массива char в String и без использования
 *  стандартных методов класса String.
 *   Вернуть i-ый символ строки.
 *   Вернуть длину строки.
 *   Вывести строку на экран
 *   Проверить, есть ли переданная подстрока в AmazingString (на вход
 * подается массив char). Вернуть true, если найдена и false иначе
 *   Проверить, есть ли переданная подстрока в AmazingString (на вход
 * подается String). Вернуть true, если найдена и false иначе
 *   Удалить из строки AmazingString ведущие пробельные символы, если
 * они есть.
 *   Развернуть строку (первый символ должен стать последним, а
 * последний первым и т.д.)
 */
public class AmazingString {

    private int lengthA;

    private char indexA;

    private String reversA = "";

    private String trimA = "";

    private char[] ch;
    private String str = "";


    public AmazingString(char[] ch) {
        this.ch = ch;
    }

    // на данном этапе обучения нет возможности реализовать
    // метод charAt(), поэтому буду использовать его
    public AmazingString(String str) {

        char[] c = new char[str.length()];
        for (int i = 0; i < str.length(); i++) {

            c[i] = str.charAt(i);
        }
        this.ch = c;
    }


    /**
     * Метод lengthA(), который возвращает длину массива символов.
     */
    public int lengthA() {

        for (char c : this.ch) {

            this.lengthA++;
        }
        return this.lengthA;
    }


    /**
     * Метод indexA(), который выводит символ под индексом указанным
     * во входных данных.
     */
    public char indexA(int i) {

        for (int j = 0; j < lengthA; j++) {
            if (j == i) {

                indexA = this.ch[j];
            }
        }
        return indexA;
    }


    /**
     * Метод substringA(), проверяет есть ли переданная подстрока в
     * AmazingString (на вход подается массив char). Вернуть true, если
     * найдена и false иначе
     */
    // beginIndex – начальный индекс, включительно;
    //endIndex – конечный индекс, не включая.
    public boolean substringA(char[] c) {

        int beginIndex = 0;
        int endIndex = 0;
        int count = 0;

        // находим длину входного массива
        for (char q : c) {

            count++;
        }

        // если длинна входного массива больше вернуть false
        if (count > this.lengthA) return false;

        // находим первый индекс совпадения символов с входным массивом
        // отбрасывая пробельные символы
        for (int i = 0; i < this.lengthA; i++) {

            if (this.ch[i] == ' ') continue;
            if (c[0] == this.ch[i]) {

                beginIndex = i;
            }
        }

        // находим последний индекс совпадения символов с входным массивом
//        for (int i = this.lengthA - 1; i >= 0; i--) {
//            if(c[count - 1] == this.ch[i]){
//                endIndex = i;
//            }
//        }
        // создаем новый массив и заполняем его символами массива ch
        // начиная с символа под индексом beginIndex и увеличивая их на i
        char[] w = new char[count];
        for (int i = 0; i < count; i++) {

            w[i] = this.ch[beginIndex + i];
        }

        // сравниваем проходя по индексам нового и входного массива
        // на совпадения всех символов
        for (int i = 0; i < count; i++) {
            if (w[i] != c[i]) {

                return false;
            }
        }
        System.out.println("нового " + Arrays.toString(w));
        return true;
    }


    /**
     * Метод substringA(), проверяет есть ли переданная подстрока в
     * AmazingString (на вход подается String). Вернуть true, если
     * найдена и false иначе
     */
    public boolean substringA(String str) {

        char[] c = new char[str.length()];
        for (int i = 0; i < str.length(); i++) {

            c[i] = str.charAt(i);
        }
        return substringA(c);
    }


    /**
     * Метод stringA(), который вывод строку.
     */
    public String stringA() {

        for (int i = 0; i < lengthA; i++) {

            this.str += this.ch[i];
        }
        return this.str;
    }


    /**
     * Метод trimA(), который удаляет из строки ведущие пробельные
     * символы.
     */
    public String trimA() {

        for (int i = 0; i < lengthA; i++) {

            if (ch[i] == ' ') continue;
            this.trimA += this.ch[i];
        }
        return this.trimA;
    }


    /**
     * Метод reversA(), который  разворачивает массив символов.
     */
    public String reversA() {

        for (int i = lengthA - 1; i >= 0; i--) {

            this.reversA += this.ch[i];
        }
        return this.reversA;
    }
//    public static int lengthB(String str) {
//
//        str += '\0';
//        int count = 0;
//
//        for (int i = 0; str.charAt(i) != '\0'; i++) {
//            count++;
//        }
//        return count;
//    }
}
