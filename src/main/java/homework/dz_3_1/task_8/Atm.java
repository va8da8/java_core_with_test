package homework.dz_3_1.task_8;


/*
 * Реализовать класс “банкомат” Atm.
 * Класс должен:
 *   Содержать конструктор, позволяющий задать курс валют перевода
 *  долларов в
 * рубли и курс валют перевода рублей в доллары (можно выбрать и
 *  задать любые
 * положительные значения).
 *   Содержать два публичных метода, которые позволяют переводить
 *  переданную
 * сумму рублей в доллары и долларов в рубли.
 *   Хранить приватную переменную счетчик — количество созданных инстансов
 * класса Atm и публичный метод, возвращающий этот счетчик (подсказка:
 * реализуется через static)
 */
public class Atm {

    private static double roubles;

    private int rouble;

    private static double dollars;

    private int dollar;

    private static int count;


    /**
     * Конструктор Atm позволяющий задать курс валют перевода долларов в
     * рубли и курс валют перевода рублей в доллары
     */
    public Atm(int dollar, double roubles, int rouble, double dollars) {

        setDollar(dollar);
        setRoubles(roubles);
        setRouble(rouble);
        setDollars(dollars);
    }


    /**
     * Метод rub, который переводит переданную сумму долларов в рубли.
     */
    public double rub(double dollars) {

        roubles = dollars * getRoubles();
        return roubles;
    }


    /**
     * Метод dol, который переводит переданную сумму рублей в доллары.
     */
    public double dol(double roubles) {

        dollars = roubles / getRoubles();
        return dollars;
    }


    /**
     * Метод sumCount(), который возвращает количество созданных
     * инстансов класса Atm.
     */
    public static int sumCount() { return count; }


    /**
     * Методы гет и сет
     */
    public double getRoubles() {
        return roubles;
    }

    public void setRoubles(double roubles) {
        this.roubles = roubles;
    }

    public int getRouble() {
        return rouble;
    }

    public void setRouble(int rouble) {
        this.rouble = rouble;
    }

    public double getDollars() {
        return dollars;
    }

    public void setDollars(double dollars) {
        this.dollars = dollars;
    }

    public int getDollar() {
        return dollar;
    }

    public void setDollar(int dollar) {
        this.dollar = dollar;
    }
}
