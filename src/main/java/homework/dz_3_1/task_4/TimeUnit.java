package homework.dz_3_1.task_4;


import java.util.Calendar;

/*
* Необходимо реализовать класс TimeUnit с функционалом, описанным ниже
* (необходимые поля продумать самостоятельно). Обязательно должны быть
* реализованы валидации на входные параметры.
Конструкторы:
*   Возможность создать TimeUnit, задав часы, минуты и секунды.
*   Возможность создать TimeUnit, задав часы и минуты. Секунды тогда
* должны проставиться нулевыми.
*   Возможность создать TimeUnit, задав часы. Минуты и секунды тогда
* должны проставиться нулевыми.
* Публичные методы:
*   Вывести на экран установленное в классе время в формате hh:mm:ss
*   Вывести на экран установленное в классе время в 12-часовом формате
* (используя hh:mm:ss am/pm)
*   Метод, который прибавляет переданное время к установленному в
* TimeUnit (на вход передаются только часы, минуты и секунды).
*/
public class TimeUnit {

    private int hours;

    private int minutes;

    private int seconds;


    public TimeUnit(int hours, int minutes, int seconds) {
        setHours(hours);
        setMinutes(minutes);
        setSeconds(seconds);
    }

    public TimeUnit(int hours, int minutes) {

        this(hours, minutes, 0);
    }

    public TimeUnit(int hours) {

        this(hours, 0, 0);
    }


    /**
     * Методы get- и set- (в методах set- выполнена валидация на
     * входные параметры).
     */
    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        if (hours < 0 || hours > 23)
            this.hours = 0;
        else
            this.hours = hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        if (minutes < 0 || minutes > 59)
            this.minutes = 0;
        else
            this.minutes = minutes;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {

        if (seconds < 0 || seconds > 59)
            this.seconds = 0;
        else
            this.seconds = seconds;
    }
//    public static TimeUnit getInstance
//            (int hours, int minutes, int seconds){
//        if(valid(hours, minutes, seconds)){
//            return new TimeUnit();
//        } else
//            return null;
//    }
//    private static boolean valid(int hours, int minutes, int seconds) {
//        if(hours > 23 || hours < 0)
//            return false;
//        if(minutes > 59 || minutes < 0)
//            return false;
//        if(seconds > 59 || seconds < 0)
//            return false;
//        return true;
//    }


    /**
     * Метод soutTime(), который выводит время в формате hh:mm:ss.
     */
    public void soutTime() {

        System.out.printf("%02d:%02d:%02d%n", hours, minutes, seconds);
    }


    /**
     * Метод plusTime(), который прибавляет переданное время к
     * установленному на вход передаются только часы, минуты и секунды.
     */
    public void plusTime(int hours, int minutes, int seconds) {

        this.hours += hours;
        this.minutes += minutes;
        this.seconds += seconds;
    }


    /**
     * Метод amPm(), выводит время в 12-часовом формате.
     */
    public void amPm() {
        String strAm = " Am";
        String strPm = " Pm";
        if (hours >= 0 && hours < 12) {

            System.out.printf("%02d:%02d:%02d%s%n",
                    hours, minutes, seconds, strAm);
        } else System.out.printf("%02d:%02d:%02d%s%n",
                hours %= 12, minutes, seconds, strPm);
    }
}
