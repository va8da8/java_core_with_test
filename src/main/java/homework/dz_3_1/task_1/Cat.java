package homework.dz_3_1.task_1;


/*
 * Необходимо реализовать класс Cat.
 * У класса должны быть реализованы следующие приватные методы:
 *   sleep() — выводит на экран “Sleep”
 *   meow() — выводит на экран “Meow”
 *   eat() — выводит на экран “Eat”
 * И публичный метод:
 *   status() — вызывает один из приватных методов случайным образом.
 */
class Cat {


    /**
     * Приватный метод sleep() который выводит сообщение "Sleep".
     */
    private void sleep() {

        System.out.println("Sleep");
    }


    /**
     * Приватный метод meow() который выводит сообщение "Meow".
     */
    private void meow() {

        System.out.println("Meow");
    }


    /**
     * Приватный метод eat() который выводит сообщение "Eat".
     */
    private void eat() {

        System.out.println("Eat");
    }


    /**
     * Публичный метод status(), который случайным образом обращается
     * к приватным методам этого класса и вызывает их по одному.
     */
    public void status() {
        for (int i = 0; i < 1; i++) {
            int rand = (int) (3 * Math.random());
            switch (rand) {
                case 0:
                    sleep();
                    break;
                case 1:
                    meow();
                    break;
                case 2:
                    eat();
                    break;
            }
        }
    }
}
