package homework.dz_3_1.task_5;


/*
 * Необходимо реализовать класс DayOfWeek для хранения порядкового номера
 * дня недели (byte) и названия дня недели (String).
 * Затем в отдельном классе в методе main создать массив объектов DayOfWeek
 * длины 7. Заполнить его соответствующими значениями
 * (от 1 Monday до 7 Sunday) и вывести значения массива объектов DayOfWeek
 * на экран.
 * Пример вывода:
 *   1 Monday
 *   2 Tuesday
 *   ...
 *   7 Sunday
 */
class DayOfWeek {

    private byte numDay;

    private String strDay;

    private byte[] numDayOfWeek = {1, 2, 3, 4, 5, 6, 7};

    private String[] strDayOfWeek = {"Monday", "Tuesday", "Wednesday",
            "Thursday", "Friday", "Saturday", "Sunday"};


    /**
     * Конструктор класса DayOfWeek который принимает на вход два
     * значения типа (int), для автоматического создания объектов
     * DayOfWeek в цикле for с элементами массивов numDayOfWeek и
     * strDayOfWeek соответствующими индексам входных значений.
     */
    public DayOfWeek(int i, int j) {
        setNumDay(this.numDayOfWeek[i]);
        setStrDay(this.strDayOfWeek[j]);
    }


    /**
     * Два приватных метода setNumDay и setStrDay для установки значений
     * из массивов numDayOfWeek и strDayOfWeek в конструкторе DayOfWeek.
     */
    private void setNumDay(byte numDay) {
        this.numDay = numDay;
    }


    private void setStrDay(String strDay) {
        this.strDay = strDay;
    }


    /**
     * Переопределенный метод toString() для корректного вывода.
     */
    @Override
    public String toString() {
        return numDay + " " + strDay;
    }
}
