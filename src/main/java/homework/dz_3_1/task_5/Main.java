package homework.dz_3_1.task_5;


/**
 * Класс Main содержит один метод main.
 */
class Main {


    /**
     * Метод main создает массив объектов класса DayOfWeek и выводит его.
     */
    public static void main(String... args) {

        DayOfWeek[] dayOfWeeks = new DayOfWeek[7];
        for (int i = 0; i < dayOfWeeks.length; i++) {

            dayOfWeeks[i] = new DayOfWeek(i, i);
            System.out.println(dayOfWeeks[i]);
        }
    }
}
