package homework.dz_1_2.task_6;


import java.util.Scanner;


/*
 * На следующий день на работе Петю и его коллег попросили заполнить
 * анкету. Один из вопросов был про уровень владения английского. Петя
 * и его коллеги примерно представляют, сколько они знают иностранных
 * слов. Также у них есть табличка перевода количества слов в уровень
 * владения английском языком. Было бы здорово автоматизировать этот
 * перевод!
 * На вход подается положительное целое число count - количество
 * выученных иностранных слов. Нужно вывести какому уровню
 * соответствует это количество.
 * Ограничения: 0 <= n < 10000
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int count = scanner.nextInt();

        System.out.println(new Main().englishLevel(count));
    }


    public String englishLevel(int count) {

        if (count < 1 || count > 9_999) {

            throw new IllegalArgumentException
                    ("Введено недопустимое значение");
        }

        if (count < 500) {
            return ("beginner");
        }
        if (count < 1500) {
            return ("pre-intermediate");
        }
        if (count < 2500) {
            return ("intermediate");
        }
        if (count < 3500) {
            return ("upper-intermediate");
        }
        return ("fluent");
    }
}