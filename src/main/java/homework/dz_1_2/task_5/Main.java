package homework.dz_1_2.task_5;


import java.util.Scanner;


/*
 * Дома дочери Пети опять нужна помощь с математикой! В этот раз ей
 * нужно проверить, имеет ли предложенное квадратное уравнение решение
 * или нет.
 * На вход подаются три числа — коэффициенты квадратного уравнения
 * a, b, c. Нужно вывести "Решение есть", если оно есть и "Решения нет",
 * если нет.
 * Ограничения: -100 < a, b, c < 100
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();

        System.out.println(new Main().isThereSolution(a, b, c));
    }


    public String isThereSolution(int a, int b, int c) {

        if ((a < -99 || a > 99) || (b < -99 || b > 99)
                || (c < -99 || c > 99)) {

            throw new IllegalArgumentException
                    ("Введено недопустимое значение");
        }
        return ((int) (Math.pow(b, 2) - (4 * a * c)) < 0)
                ?
                "Решения нет"
                :
                "Решение есть";
    }
}