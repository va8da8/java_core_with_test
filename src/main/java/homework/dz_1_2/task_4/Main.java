package homework.dz_1_2.task_4;


import java.util.Scanner;


/*
 * После вкусного обеда Петя принимается за подсчет дней до выходных.
 * Календаря под рукой не оказалось, а если спросить у коллеги Феди,
 * то тот называет только порядковый номер дня недели, что не очень удобно.
 * Поэтому Петя решил написать программу, которая по порядковому номеру
 * дня недели выводит сколько осталось дней до субботы. А если же сегодня
 * шестой (суббота) или седьмой (воскресенье) день, то программа выводит
 * "Ура, выходные!"
 * Ограничения: 1 <= n <= 7
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        System.out.println(new Main().dayNumber(n));
    }


    public String dayNumber(int n) {

        if (n < 1 || n > 7) {

            throw new IllegalArgumentException
                    ("Введено недопустимое значение");
        }

//        switch (n) {
//            case 1 -> {
//                return String.valueOf((5));
//            }
//            case 2 -> {
//                return String.valueOf(4);
//            }
//            case 3 -> {
//                return String.valueOf(3);
//            }
//            case 4 -> {
//                return String.valueOf(2);
//            }
//            case 5 -> {
//                return String.valueOf(1);
//            }
//            default -> {
//                return ("Ура, выходные!");
//            }
//        }

        return (n > 5)
                ?
                "Ура, выходные!"
                :
                String.valueOf(6 - n);
    }
}