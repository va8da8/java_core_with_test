package homework.dz_1_2.task_dop_1;


import java.util.Scanner;


/*
 * Марата был взломан пароль. Он решил написать программу, которая
 * проверяет его пароль на сложность. В интернете он узнал, что пароль
 * должен отвечать следующим требованиям:
 *  пароль должен состоять из хотя бы 8 символов;
 *  в пароле должны быть:
 *   заглавные буквы
 *   строчные символы
 *   числа
 *   специальные знаки(_*-)
 * Если пароль прошел проверку, то программа должна вывести в консоль
 * строку пароль надежный, иначе строку: пароль не прошел проверку
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        String str = scanner.nextLine();

        System.out.println(new Main().isStrongPassword(str));
    }


    public String isStrongPassword(String str) {

        String pat =
                "(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\\*\\-\\_]).{8,}";

        if (str.matches(pat)) {
            return "пароль надежный";
        }
        return "пароль не прошел проверку";
    }
}