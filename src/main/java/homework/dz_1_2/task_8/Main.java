package homework.dz_1_2.task_8;


import java.util.Scanner;


/*
 * Раз так легко получается разделять по первому пробелу, Петя решил
 * немного изменить предыдущую программу и теперь разделять строку по
 * последнему пробелу.
 * Ограничения:
 *  В строке гарантированно есть хотя бы один пробел
 *  Первый и последний символ строки гарантированно не пробел
 *   2 < str.length() < 100
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        String str = scanner.nextLine();

        System.out.println(new Main().twoStringsLastSpace(str));
    }


    public String twoStringsLastSpace(String str) {

        if ((str.length() < 3 || str.length() > 99) || ((str.indexOf(" ") == 0) || (str.indexOf(" ") == str.length() - 1))) {
                //|| (str.indexOf("") != str.lastIndexOf(""))) {

            throw new IllegalArgumentException
                    ("Введено недопустимое значение");
        }
        return str.substring(0, str.lastIndexOf(" ")) +
                "\n" + str.substring(str.lastIndexOf(" ")).trim();
    }
}