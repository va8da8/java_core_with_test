package homework.dz_1_2.task_dop_2;


import java.util.Scanner;


/*
 * У нас есть почтовая посылка (String mailPackage). Каждая почтовая
 * посылка проходит через руки проверяющего. Работа проверяющего
 * заключается в следующем:
 *  во-первых, посмотреть не пустая ли посылка;
 *  во-вторых, проверить нет ли в ней камней или запрещенной продукции.
 * Наличие камней или запрещенной продукции указывается в самой посылке
 * в конце или в начале. Если в посылке есть камни, то будет написано
 * слово "камни!", если запрещенная продукция, то будет фраза
 * "запрещенная продукция".
 * После осмотра посылки проверяющий должен сказать:
 *  "камни в посылке" – если в посылке есть камни;
 *  "в посылке запрещенная продукция" – если в посылке есть что-то
 * запрещенное;
 *  "в посылке камни и запрещенная продукция" – если в посылке находятся
 * камни и запрещенная продукция;
 *  "все ок" – если с посылкой все хорошо.
 * Если посылка пустая, то с посылкой все хорошо.
 * Напишите программу, которая будет заменять проверяющего.
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        String mailPackage = scanner.nextLine();

        System.out.println(new Main().whatInside(mailPackage));
    }


    public String whatInside(String mailPackage) {

        String stones = "камни!";
        String prohibitedProducts = "запрещенная продукция";

        if (mailPackage.contains(stones) &&
                mailPackage.contains(prohibitedProducts)) {
            return "в посылке камни и запрещенная продукция";
        } else if (mailPackage.contains(prohibitedProducts)) {
            return "в посылке запрещенная продукция";
        } else if (mailPackage.contains(stones)) {
            return "камни в посылке";
        }
        return "все ок";
    }
}