package homework.dz_1_2.task_2;


import java.util.Scanner;


/*
 * Петя пришел домой и помогает дочке решать математику. Ей нужно
 * определить, принадлежит ли точка с указанными координатами первому
 * квадранту. Недолго думая, Петя решил автоматизировать процесс и
 * написать программу: на вход нужно принимать два целых числа
 * (координаты точки), выводить true, когда точка попала в квадрант
 * и false иначе.
 * Но сначала Петя вспомнил, что точка лежит в первом квадранте тогда,
 * когда её координаты удовлетворяют условию: x > 0 и y > 0.
 * Ограничения: -100 < x, y < 100

 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int x = scanner.nextInt();
        int y = scanner.nextInt();

        System.out.println(new Main().isTrueFirstQuadrant(x, y));
    }


    public boolean isTrueFirstQuadrant(int x, int y) {

        if ((x < -99 || x > 99) || (y < -99 || y > 99)) {

            throw new IllegalArgumentException
                    ("Введено недопустимое значение");
        }
        return x > 0 && y > 0;
    }
}