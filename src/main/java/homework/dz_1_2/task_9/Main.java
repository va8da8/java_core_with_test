package homework.dz_1_2.task_9;


import java.util.Scanner;


/*
 * Пока Петя практиковался в работе со строками, к нему подбежала его
 * дочь и спросила: "А правда ли, что тригонометрическое тождество
 * (sin^2(x)+ cos^2(x) - 1 == 0) всегда-всегда выполняется?"
 * Напишите программу, которая проверяет, что при любом x на входе
 * тригонометрическое тождество будет выполняться (то есть будет
 * выводить true при любом x).
 * Ограничения: -1000 < x < 1000
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int x = scanner.nextInt();

        System.out.println(new Main().isTrueIdentity(x));
    }


    public boolean isTrueIdentity(int x) {

        if (x < -999 || x > 999) {

            throw new IllegalArgumentException
                    ("Введено недопустимое значение");
        }
        return (int) ((Math.sin(Math.pow(x, 2)) +
                Math.cos(Math.pow(x, 2))) - 1) == 0;
    }
}