package homework.dz_1_2.task_7;


import java.util.Scanner;


/*
 * Петя недавно изучил строки в java и решил попрактиковаться с ними.
 * Ему хочется уметь разделять строку по первому пробелу. Для этого он
 * может воспользоваться методами indexOf() и substring().
 * На вход подается строка. Нужно вывести две строки, полученные из
 * входной, разделением по первому пробелу.
 * Ограничения:
 *  В строке гарантированно есть хотя бы один пробел
 *  Первый и последний символ строки гарантированно не пробел
 *   2 < str.length() < 100
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        String str = scanner.nextLine();

        System.out.print(new Main().twoStringsFirstSpace(str));
    }


    public String twoStringsFirstSpace(String str) {

        if ((str.length() < 3 || str.length() > 99)
                || ((str.indexOf(" ") == 0)
                || (str.indexOf(" ") == str.length() - 1))) {

            throw new IllegalArgumentException
                    ("Введено недопустимое значение");
        }
        return str.substring(0, str.indexOf(" ")) + "\n" +
                str.substring(str.indexOf(" ")).trim();
    }
}