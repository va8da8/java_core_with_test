package homework.dz_1_2.task_dop_3;


import java.util.Scanner;


/*
 * Старый телефон Андрея сломался, поэтому он решил приобрести новый.
 * Продавец телефонов предлагает разные варианты, но Андрея интересуют
 * только модели серии samsung или iphone. Также Андрей решил
 * рассматривать телефоны только от 50000 до 120000 рублей. Чтобы не
 * тратить время на разговоры, Андрей хочет написать программу, которая
 * поможет ему сделать выбор.
 * На вход подается строка – модель телефона и число – стоимость телефона.
 * Нужно вывести "Можно купить", если модель содержит слово samsung
 * или iphone и стоимость от 50000 до 120000 рублей включительно.
 * Иначе вывести "Не подходит".
 * Гарантируется, что в модели телефона не указано одновременно
 * несколько серий.
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        String model = scanner.nextLine();
        int price = scanner.nextInt();

        System.out.println(new Main().isBuy(model, price));
    }


    public String isBuy(String model, int price) {

        if ((model.contains("samsung") || model.contains("iphone")) &&
                (price >= 50_000 && price <= 120_000)) {
            return "Можно купить";
        }
        return "Не подходит";
    }
}