package homework.dz_1_2.task_3;


import java.util.Scanner;


/*
 * Петя снова пошел на работу. С сегодняшнего дня он решил ходить на
 * обед строго после полудня. Периодически он посматривает на часы
 * (x - час, который он увидел). Помогите Пете решить, пора ли ему
 * на обед или нет. Если время больше полудня, то вывести "Пора".
 * Иначе - “Рано”.
 * Ограничения: 0 <= n <= 23

 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int x = scanner.nextInt();

        System.out.println(new Main().lunch(x));
    }


    public String lunch(int x) {

        if (x < 0 || x > 23) {

            throw new IllegalArgumentException
                    ("Введено недопустимое значение");
        }
        return x <= 12
                ?
                "Рано"
                :
                "Пора";
    }
}