package homework.dz_1_2.task_11;


import java.util.Scanner;


/*
 * Разобравшись со своими (и не только) задачками, Петя уже собирался
 * лечь спать и отдохнуть перед очередным тяжелым рабочим днем, но
 * вдруг в тишине раздается детский шепот: "Па-а-а-а-п-a, мы забыли решить
 * ещё одну задачку! Давай проверим, можно ли из трех сторон составить
 * треугольник?". Что ж, придется написать еще одну программу,
 * связанную со школьной математикой.
 * На вход подается три целых положительных числа – длины сторон
 * треугольника. Нужно вывести true, если можно составить треугольник
 * из этих сторон и false иначе.
 * Ограничения: 0 < a, b, c < 100
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();

        System.out.println(new Main().isTrueTriangle(a, b, c));
    }


    public boolean isTrueTriangle(int a, int b, int c) {

        if ((a < 1 || a > 99) || (b < 1 || b > 99)
                || (c < 1 || c > 99)) {

            throw new IllegalArgumentException
                    ("Введено недопустимое значение");
        }
        return (a + b > c) && (a + c > b) && (b + c > a);
    }
}