package homework.dz_1_2.task_10;


import java.util.Scanner;
import java.util.stream.Stream;


/*
 * "А логарифмическое?" - не унималась дочь.
 * Напишите программу, которая проверяет, что log(e^n) == n для любого
 * вещественного n.
 * Ограничения: -500 < n < 500
 */
class Main {


    public static void main(String... args) {

//        Stream.of(new Scanner(System.in).nextDouble())
//                .map((n) -> Math.log(
//                        Math.pow(Math.E, n)) == n)
//                .forEach(System.out::println);

        Scanner scanner = new Scanner(System.in);

        double n = scanner.nextDouble();

        System.out.println(new Main().isTrueLogarithmic(n));
    }


    public boolean isTrueLogarithmic(double n) {

        if (n < -499 || n > 499) {

            throw new IllegalArgumentException
                    ("Введено недопустимое значение");
        }
        return Math.log(Math.pow(Math.E, n)) == n;
    }
}