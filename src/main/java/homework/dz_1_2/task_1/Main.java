package homework.dz_1_2.task_1;


import java.util.Scanner;


/*
 * За каждый год работы Петя получает на ревью оценку. На вход подаются
 * оценки Пети за последние три года (три целых положительных числа).
 * Если последовательность оценок строго монотонно убывает, то вывести
 * "Петя, пора трудиться"
 * В остальных случаях вывести "Петя молодец!"
 * Ограничения: 0 < a, b, c < 100
 */
class Main {


    public static void main(String... args) {

        Scanner scanner = new Scanner(System.in);

        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();

        System.out.println(new Main().grade(a, b, c));
    }


    public String grade(int a, int b, int c) {

        if ((a < 1 || a > 99) || (b < 1 || b > 99) || (c < 1 || c > 99)) {

            throw new IllegalArgumentException
                    ("Введено недопустимое значение");
        }

//        if (a > b && b > c) {
//            return "Петя, пора трудиться";
//        } else {
//            return "Петя молодец!";
//        }

        return (a > b && b > c)
                ?
                "Петя, пора трудиться"
                :
                "Петя молодец!";
    }
}